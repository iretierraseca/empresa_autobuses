<%-- 
    Document   : vistaTarjetaLogin
    Created on : 03-mar-2019, 9:27:35
    Author     : Irene
--%>

<%@page import="MODELO.AES"%>
<%@page import="POJO.Tarjeta"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Set"%>
<%@page import="POJO.Cliente"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design Bootstrap</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.7.0.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/tarjeta.css" rel="stylesheet">


    </head>

    <body>
        <%
            Cliente objCliente = (Cliente) session.getAttribute("cliente");
            Set <Tarjeta> arrayTarjetas = (Set<Tarjeta>) objCliente.getTarjetas();
            
            String nombreApellidos = objCliente.getNombreCliente() + " " + objCliente.getApellidosCliente();
            
            
        %>



        <!-- Main navigation -->
        <main class="mt-5">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
                <div class="container">
                    <a class="navbar-brand" href="#"><strong>TerraBus</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <!-- Horizontal Steppers -->
                <div class="row">
                    <div class="col-md-12">

                        <!-- Stepers Wrapper -->
                        <ul class="stepper stepper-horizontal">

                            <!-- First Step -->
                            <li class="completed">
                                <a href="#!">
                                    <span class="circle">1</span>
                                    <span class="label">Elige tu horario</span>
                                </a>
                            </li>

                            <!-- Second Step -->
                            <li class="active">
                                <a href="#!">
                                    <span class="circle">2</span>
                                    <span class="label">Personalizar asientos</span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#!">
                                    <span class="circle">3</span>
                                    <span class="label">Resumen compra</span>
                                </a>
                            </li>

                            <!-- Third Step -->
                            <li class="active" style="background-color: rgba(0,0,0,0.2);">
                                <a href="#!">
                                    <span class="circle">4</span>
                                    <span class="label">Finalizar compra</span>
                                </a>
                            </li>

                        </ul>
                        <!-- /.Stepers Wrapper -->

                    </div>
                </div>

            </div>

            <div class="container">
                <form action="../controladorPagarLogin" id="formularioTarjetaL" onsubmit="return validarFormulario();" >

                <div class="payment-title">
                    <h1>Información de pago</h1>
                </div>
                <div class="container d-inline-flex">
                    <div class="containerTarjeta preload">
                        <div class="creditcard">
                            <div class="front">
                                <div id="ccsingle"></div>
                                <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                <g id="Front">
                                <g id="CardBackground">
                                <g id="Page-1_1_">
                                <g id="amex_1_">
                                <path id="Rectangle-1_1_" class="lightcolor grey" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                      C0,17.9,17.9,0,40,0z" />
                                </g>
                                </g>
                                <path id="Rectangle-1_2_" class="darkcolor greydark" d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z" />
                                </g>
                                <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber" class="st2 st3 st4">0123 4567 8910 1112</text>
                                <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname" class="st2 st5 st6"><%=nombreApellidos%></text>
                                <text transform="matrix(1 0 0 1 54.1074 389.8793)" class="st7 st5 st8">cardholder name</text>
                                <text transform="matrix(1 0 0 1 479.7754 388.8793)" class="st7 st5 st8">expiration</text>
                                <text transform="matrix(1 0 0 1 65.1054 241.5)" class="st7 st5 st8" >card number</text>
                                <g>
                                <text transform="matrix(1 0 0 1 574.4219 433.8095)" id="svgexpire" class="st2 st5 st9">01/23</text>
                                <text transform="matrix(1 0 0 1 479.3848 417.0097)" class="st2 st10 st11">VALID</text>
                                <text transform="matrix(1 0 0 1 479.3848 435.6762)" class="st2 st10 st11">THRU</text>
                                <polygon class="st2" points="554.5,421 540.4,414.2 540.4,427.9 		" />
                                </g>
                                <g id="cchip">
                                <g>
                                <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                                      c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z" />
                                </g>
                                <g>
                                <g>
                                <rect x="82" y="70" class="st12" width="1.5" height="60" />
                                </g>
                                <g>
                                <rect x="167.4" y="70" class="st12" width="1.5" height="60" />
                                </g>
                                <g>
                                <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                                      c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                                      C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                                      c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                                      c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z" />
                                </g>
                                <g>
                                <rect x="82.8" y="82.1" class="st12" width="25.8" height="1.5" />
                                </g>
                                <g>
                                <rect x="82.8" y="117.9" class="st12" width="26.1" height="1.5" />
                                </g>
                                <g>
                                <rect x="142.4" y="82.1" class="st12" width="25.8" height="1.5" />
                                </g>
                                <g>
                                <rect x="142" y="117.9" class="st12" width="26.2" height="1.5" />
                                </g>
                                </g>
                                </g>
                                </g>
                                <g id="Back">
                                </g>
                                </svg>
                            </div>
                            <div class="back">
                                <svg version="1.1" id="cardback" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                <g id="Front">
                                <line class="st0" x1="35.3" y1="10.4" x2="36.7" y2="11" />
                                </g>
                                <g id="Back">
                                <g id="Page-1_2_">
                                <g id="amex_2_">
                                <path id="Rectangle-1_2_" class="darkcolor greydark" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                      C0,17.9,17.9,0,40,0z" />
                                </g>
                                </g>
                                <rect y="61.6" class="st2" width="750" height="78" />
                                <g>
                                <path class="st3" d="M701.1,249.1H48.9c-3.3,0-6-2.7-6-6v-52.5c0-3.3,2.7-6,6-6h652.1c3.3,0,6,2.7,6,6v52.5
                                      C707.1,246.4,704.4,249.1,701.1,249.1z" />
                                <rect x="42.9" y="198.6" class="st4" width="664.1" height="10.5" />
                                <rect x="42.9" y="224.5" class="st4" width="664.1" height="10.5" />
                                <path class="st5" d="M701.1,184.6H618h-8h-10v64.5h10h8h83.1c3.3,0,6-2.7,6-6v-52.5C707.1,187.3,704.4,184.6,701.1,184.6z" />
                                </g>
                                <text transform="matrix(1 0 0 1 621.999 227.2734)" id="svgsecurity" class="st6 st7">000</text>
                                <g class="st8">
                                <text transform="matrix(1 0 0 1 518.083 280.0879)" class="st9 st6 st10">security code</text>
                                </g>
                                <rect x="58.1" y="378.6" class="st11" width="375.5" height="13.5" />
                                <rect x="58.1" y="405.6" class="st11" width="421.7" height="13.5" />
                                <text transform="matrix(1 0 0 1 59.5073 228.6099)" id="svgnameback" class="st12 st13"><%=nombreApellidos%></text>
                                </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="form-container">
                        <div class="field-container">
                            <label for="name">Name</label>
                            <input id="name" name="name" maxlength="20" type="text" value="<%=nombreApellidos%>" readonly>
                        </div>

                        <div class="field-container">
                            <label for="cardnumber">Card Number</label>
                            <input id="cardnumber" name="cardnumber" type="text"  inputmode="numeric" onclick="comprobarTexto(this)" onblur="comprobarNumero(this)">
                            <svg id="ccicon" class="ccicon" width="750" height="471" viewBox="0 0 750 471" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                            </svg>
                        </div>
                        <div class="field-container">
                            <label for="expirationdate">Expiration (mm/yy)</label>
                            <input id="expirationdate" name="expirationdate" type="text" inputmode="numeric">
                        </div>
                        <div class="field-container">
                            <label for="securitycode">Security Code</label>
                            <input id="securitycode" name="securitycode" type="text"  inputmode="numeric">
                        </div>
                    </div>
                    <div style="width:350px">
                        <div style="width: 350px; margin-top: 20px; ">
                            <label for="selectTarjeta">Lista de tarjetas</label>
                            <select onchange="valorTarjeta()" id="selectTarjeta" name="selectTarjeta" class="browser-default custom-select" style="margin-top: 3px; font-size: 16px;width: 100%;border-radius: 3px;border: 1px solid #dcdcdc;height: 56px;">
                                <%if(arrayTarjetas.size()==0){%>
                                
                                <%}else{%>
                                <option value="">Tarjetas asociadas</option>
                                    <%
                                    for(Tarjeta item:arrayTarjetas){%>
                                    <option value="<%=AES.decrypt(item.getNumeroTarjeta(),"tierraseca")%>"><%="**** **** **** **" + (AES.decrypt(item.getNumeroTarjeta(),"tierraseca")).substring(17,19)%></option>
                                   <% }
                                }%>
                                
                            </select>
                        </div>
                        <br>    
                        <div style="width: 350px; margin-top: 20px; ">
                            <label for="tipoTarjeta">Tipo de tarjeta</label>
                            <div class="form-check">
                                <i class="fab fa-cc-visa" style="font-size:35px"></i>
                                <input type="radio" class="form-check-input tipoTarjeta" id="VISA" name="radioTipoTarjeta" value="VISA" required>
                                <label class="form-check-label" for="VISA">VISA</label>
                            </div>
                            <div class="form-check">
                                <i class="fab fa-cc-mastercard" style="font-size:35px"></i>
                                <input type="radio" class="form-check-input tipoTarjeta" id="MASTERCARD" name="radioTipoTarjeta" value="MASTERCARD" required>
                                <label class="form-check-label" for="MASTERCARD">MASTERCARD</label>
                            </div>
                            
                        </div>
                    </div>

                </div>
                 <div class="container" style="text-align: center;">
                    <button class="btn peach-gradient" id="botonPagar">Pagar</button>
                </div> 
                </form>                
            </div>
                               

        </main>



        <!-- SCRIPTS -->
        



        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/imask/3.4.0/imask.min.js'></script>
        <script type='text/javascript' src='../js/tarj.js'></script>
        <script></script>
        <!-- JQuery -->
        <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="../js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="../js/mdb.min.js"></script>

        <script type='text/javascript' src='../js/compiled.0.min.js'></script>
        

        <script>
            let visa_single = `<svg version="1.1" id="Layer_1" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="750px" height="471px" viewBox="0 0 750 471" enable-background="new 0 0 750 471" xml:space="preserve"><title>Slice 1</title><desc>Created with Sketch.</desc><g id="visa" sketch:type="MSLayerGroup"><path id="Shape" sketch:type="MSShapeGroup" fill="#0E4595" d="M278.198,334.228l33.36-195.763h53.358l-33.384,195.763H278.198L278.198,334.228z"/><path id="path13" sketch:type="MSShapeGroup" fill="#0E4595" d="M524.307,142.687c-10.57-3.966-27.135-8.222-47.822-8.222c-52.725,0-89.863,26.551-90.18,64.604c-0.297,28.129,26.514,43.821,46.754,53.185c20.77,9.597,27.752,15.716,27.652,24.283c-0.133,13.123-16.586,19.116-31.924,19.116c-21.355,0-32.701-2.967-50.225-10.274l-6.877-3.112l-7.488,43.823c12.463,5.466,35.508,10.199,59.438,10.445c56.09,0,92.502-26.248,92.916-66.884c0.199-22.27-14.016-39.216-44.801-53.188c-18.65-9.056-30.072-15.099-29.951-24.269c0-8.137,9.668-16.838,30.559-16.838c17.447-0.271,30.088,3.534,39.936,7.5l4.781,2.259L524.307,142.687"/><path id="Path" sketch:type="MSShapeGroup" fill="#0E4595" d="M661.615,138.464h-41.23c-12.773,0-22.332,3.486-27.941,16.234l-79.244,179.402h56.031c0,0,9.16-24.121,11.232-29.418c6.123,0,60.555,0.084,68.336,0.084c1.596,6.854,6.492,29.334,6.492,29.334h49.512L661.615,138.464L661.615,138.464z M596.198,264.872c4.414-11.279,21.26-54.724,21.26-54.724c-0.314,0.521,4.381-11.334,7.074-18.684l3.607,16.878c0,0,10.217,46.729,12.352,56.527h-44.293V264.872L596.198,264.872z"/><path id="path16" sketch:type="MSShapeGroup" fill="#0E4595" d="M232.903,138.464L180.664,271.96l-5.565-27.129c-9.726-31.274-40.025-65.157-73.898-82.12l47.767,171.204l56.455-0.064l84.004-195.386L232.903,138.464"/><path id="path18" sketch:type="MSShapeGroup" fill="#F2AE14" d="M131.92,138.464H45.879l-0.682,4.073c66.939,16.204,111.232,55.363,129.618,102.415l-18.709-89.96C152.877,142.596,143.509,138.896,131.92,138.464"/></g></svg>`;
            let mastercard_single = `<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="482.51" height="374" viewBox="0 0 482.51 374"> <title>mastercard</title> <g> <path d="M220.13,421.67V396.82c0-9.53-5.8-15.74-15.32-15.74-5,0-10.35,1.66-14.08,7-2.9-4.56-7-7-13.25-7a14.07,14.07,0,0,0-12,5.8v-5h-7.87v39.76h7.87V398.89c0-7,4.14-10.35,9.94-10.35s9.11,3.73,9.11,10.35v22.78h7.87V398.89c0-7,4.14-10.35,9.94-10.35s9.11,3.73,9.11,10.35v22.78Zm129.22-39.35h-14.5v-12H327v12h-8.28v7H327V408c0,9.11,3.31,14.5,13.25,14.5A23.17,23.17,0,0,0,351,419.6l-2.49-7a13.63,13.63,0,0,1-7.46,2.07c-4.14,0-6.21-2.49-6.21-6.63V389h14.5v-6.63Zm73.72-1.24a12.39,12.39,0,0,0-10.77,5.8v-5h-7.87v39.76h7.87V399.31c0-6.63,3.31-10.77,8.7-10.77a24.24,24.24,0,0,1,5.38.83l2.49-7.46a28,28,0,0,0-5.8-.83Zm-111.41,4.14c-4.14-2.9-9.94-4.14-16.15-4.14-9.94,0-16.15,4.56-16.15,12.43,0,6.63,4.56,10.35,13.25,11.6l4.14.41c4.56.83,7.46,2.49,7.46,4.56,0,2.9-3.31,5-9.53,5a21.84,21.84,0,0,1-13.25-4.14l-4.14,6.21c5.8,4.14,12.84,5,17,5,11.6,0,17.81-5.38,17.81-12.84,0-7-5-10.35-13.67-11.6l-4.14-.41c-3.73-.41-7-1.66-7-4.14,0-2.9,3.31-5,7.87-5,5,0,9.94,2.07,12.43,3.31Zm120.11,16.57c0,12,7.87,20.71,20.71,20.71,5.8,0,9.94-1.24,14.08-4.56l-4.14-6.21a16.74,16.74,0,0,1-10.35,3.73c-7,0-12.43-5.38-12.43-13.25S445,389,452.07,389a16.74,16.74,0,0,1,10.35,3.73l4.14-6.21c-4.14-3.31-8.28-4.56-14.08-4.56-12.43-.83-20.71,7.87-20.71,19.88h0Zm-55.5-20.71c-11.6,0-19.47,8.28-19.47,20.71s8.28,20.71,20.29,20.71a25.33,25.33,0,0,0,16.15-5.38l-4.14-5.8a19.79,19.79,0,0,1-11.6,4.14c-5.38,0-11.18-3.31-12-10.35h29.41v-3.31c0-12.43-7.46-20.71-18.64-20.71h0Zm-.41,7.46c5.8,0,9.94,3.73,10.35,9.94H364.68c1.24-5.8,5-9.94,11.18-9.94ZM268.59,401.79V381.91h-7.87v5c-2.9-3.73-7-5.8-12.84-5.8-11.18,0-19.47,8.7-19.47,20.71s8.28,20.71,19.47,20.71c5.8,0,9.94-2.07,12.84-5.8v5h7.87V401.79Zm-31.89,0c0-7.46,4.56-13.25,12.43-13.25,7.46,0,12,5.8,12,13.25,0,7.87-5,13.25-12,13.25-7.87.41-12.43-5.8-12.43-13.25Zm306.08-20.71a12.39,12.39,0,0,0-10.77,5.8v-5h-7.87v39.76H532V399.31c0-6.63,3.31-10.77,8.7-10.77a24.24,24.24,0,0,1,5.38.83l2.49-7.46a28,28,0,0,0-5.8-.83Zm-30.65,20.71V381.91h-7.87v5c-2.9-3.73-7-5.8-12.84-5.8-11.18,0-19.47,8.7-19.47,20.71s8.28,20.71,19.47,20.71c5.8,0,9.94-2.07,12.84-5.8v5h7.87V401.79Zm-31.89,0c0-7.46,4.56-13.25,12.43-13.25,7.46,0,12,5.8,12,13.25,0,7.87-5,13.25-12,13.25-7.87.41-12.43-5.8-12.43-13.25Zm111.83,0V366.17h-7.87v20.71c-2.9-3.73-7-5.8-12.84-5.8-11.18,0-19.47,8.7-19.47,20.71s8.28,20.71,19.47,20.71c5.8,0,9.94-2.07,12.84-5.8v5h7.87V401.79Zm-31.89,0c0-7.46,4.56-13.25,12.43-13.25,7.46,0,12,5.8,12,13.25,0,7.87-5,13.25-12,13.25C564.73,415.46,560.17,409.25,560.17,401.79Z" transform="translate(-132.74 -48.5)"/> <g> <rect x="169.81" y="31.89" width="143.72" height="234.42" fill="#ff5f00"/> <path d="M317.05,197.6A149.5,149.5,0,0,1,373.79,80.39a149.1,149.1,0,1,0,0,234.42A149.5,149.5,0,0,1,317.05,197.6Z" transform="translate(-132.74 -48.5)" fill="#eb001b"/> <path d="M615.26,197.6a148.95,148.95,0,0,1-241,117.21,149.43,149.43,0,0,0,0-234.42,148.95,148.95,0,0,1,241,117.21Z" transform="translate(-132.74 -48.5)" fill="#f79e1b"/> </g> </g></svg>`;
            var inputIcono = document.getElementById("ccsingle");

           function comprobarTexto(event){
               event.value = "";
               document.getElementById("expirationdate").value = "";
               var tipoTarjetas = document.getElementsByClassName("tipoTarjeta");
               
               
               for(var i=0; i<tipoTarjetas.length;i++){
                 
                   if(tipoTarjetas[i].hasAttribute("checked")){
                       tipoTarjetas[i].removeAttribute("checked");
                   }
                   if(tipoTarjetas[i].hasAttribute("disabled")){
                       tipoTarjetas[i].removeAttribute("disabled");
                   }
                   
               }
               
               document.getElementById("VISA").checked = false;
                    document.getElementById("MASTERCARD").checked = false;
                    document.getElementById("selectTarjeta").value = "";
                    
                     document.getElementById("Rectangle-1_1_").classList.remove("grey");
                   document.getElementById("Rectangle-1_1_").classList.remove("lime");
                   document.getElementById("Rectangle-1_1_").classList.remove("lightblue");
                   document.getElementById("Rectangle-1_1_").classList.add("grey");
                   
                   
                   document.getElementById("Rectangle-1_2_").classList.remove("greydark");
                   document.getElementById("Rectangle-1_2_").classList.remove("limedark");
                   document.getElementById("Rectangle-1_2_").classList.remove("lightbluedark");
                   document.getElementById("Rectangle-1_2_").classList.add("greydark");
                   
                   inputIcono.innerHTML = "";
           }
           
           function comprobarNumero(event){
               var longitud = event.value.length;//cuando sea 18
               var numeroTarjeta = event.value;
               
               
               if(longitud == 19){
                   //Comprobacion tarjeta
                var reVisa = "^4[0-9]{12}(?:[0-9]{3})?$";
                var reMasterCard = "^5[1-5][0-9]{14}$";
                var inputCreditCard = document.getElementById("cardnumber");
                var crediccard = inputCreditCard.value;
                var numeroTarjeta = crediccard.replace(/ /g, "");
                
                
                if(numeroTarjeta.match(reVisa)){
                   //alert("Tarjeta visa"); 
                    document.getElementById("Rectangle-1_1_").classList.remove("grey");
                   document.getElementById("Rectangle-1_1_").classList.remove("lime");
                   document.getElementById("Rectangle-1_1_").classList.remove("lightblue");
                   document.getElementById("Rectangle-1_1_").classList.add("lime");
                   
                   
                   document.getElementById("Rectangle-1_2_").classList.remove("greydark");
                   document.getElementById("Rectangle-1_2_").classList.remove("limedark");
                   document.getElementById("Rectangle-1_2_").classList.remove("lightbluedark");
                   document.getElementById("Rectangle-1_2_").classList.add("limedark");
                   
                   if (document.getElementById("VISA").hasAttribute("disabled")) {
                    document.getElementById("VISA").removeAttribute("disabled");
                    }
                   document.getElementById("VISA").checked = true;
                   document.getElementById("MASTERCARD").setAttribute("disabled","");
                   inputIcono.innerHTML = "";
                   inputIcono.innerHTML = visa_single;
 
                }else if(numeroTarjeta.match(reMasterCard)){
                    
                    if (document.getElementById("MASTERCARD").hasAttribute("disabled")) {
                    document.getElementById("MASTERCARD").removeAttribute("disabled");
                    }
                   document.getElementById("MASTERCARD").checked = true;
                   document.getElementById("VISA").setAttribute("disabled","");
                    
                    document.getElementById("Rectangle-1_1_").classList.remove("grey");
                   document.getElementById("Rectangle-1_1_").classList.remove("lime");
                   document.getElementById("Rectangle-1_1_").classList.remove("lightblue");
                   document.getElementById("Rectangle-1_1_").classList.add("lightblue");
                   
                   
                   document.getElementById("Rectangle-1_2_").classList.remove("greydark");
                   document.getElementById("Rectangle-1_2_").classList.remove("limedark");
                   document.getElementById("Rectangle-1_2_").classList.remove("lightbluedark");
                   document.getElementById("Rectangle-1_2_").classList.add("lightbluedark");
                   
                   inputIcono.innerHTML = "";
                   inputIcono.innerHTML = mastercard_single;
                }
               }
           }
           
           function valorTarjeta(){
               var numeroTarjeta = document.getElementById("selectTarjeta").value;
               
               if(document.getElementById("cardnumber").value != ""){
                  document.getElementById("cardnumber").value ="";
                  
               }
               document.getElementById("cardnumber").value = numeroTarjeta;
               document.getElementById('svgnumber').innerHTML = numeroTarjeta;
               
                     
               var reVisa = "^4[0-9]{12}(?:[0-9]{3})?$";
                var reMasterCard = "^5[1-5][0-9]{14}$";
                var inputCreditCard = document.getElementById("cardnumber");
                var crediccard = inputCreditCard.value;
                var numeroTarjeta = crediccard.replace(/ /g, "");
                
                
                if(numeroTarjeta.match(reVisa)){
                   //alert("Tarjeta visa"); 
                   
                   //document.getElementById("tipoCreditCard").value = tipoCard;
                   if (document.getElementById("VISA").hasAttribute("disabled")) {
                    document.getElementById("VISA").removeAttribute("disabled");
                    }
                   document.getElementById("VISA").checked = true;
                   document.getElementById("MASTERCARD").setAttribute("disabled","");
                   
                   document.getElementById("Rectangle-1_1_").classList.remove("grey");
                   document.getElementById("Rectangle-1_1_").classList.remove("lime");
                   document.getElementById("Rectangle-1_1_").classList.remove("lightblue");
                   document.getElementById("Rectangle-1_1_").classList.add("lime");
                   
                   
                   document.getElementById("Rectangle-1_2_").classList.remove("greydark");
                   document.getElementById("Rectangle-1_2_").classList.remove("limedark");
                   document.getElementById("Rectangle-1_2_").classList.remove("lightbluedark");
                   document.getElementById("Rectangle-1_2_").classList.add("limedark");
                   
                   inputIcono.innerHTML = "";
                   inputIcono.innerHTML = visa_single;
                   
                }else if(numeroTarjeta.match(reMasterCard)){
                    if (document.getElementById("MASTERCARD").hasAttribute("disabled")) {
                    document.getElementById("MASTERCARD").removeAttribute("disabled");
                    }
                   document.getElementById("MASTERCARD").checked = true;
                   document.getElementById("VISA").setAttribute("disabled","");
                   
                   document.getElementById("Rectangle-1_1_").classList.remove("grey");
                   document.getElementById("Rectangle-1_1_").classList.remove("lime");
                   document.getElementById("Rectangle-1_1_").classList.remove("lightblue");
                   document.getElementById("Rectangle-1_1_").classList.add("lightblue");
                   
                   document.getElementById("Rectangle-1_2_").classList.remove("greydark");
                   document.getElementById("Rectangle-1_2_").classList.remove("limedark");
                   document.getElementById("Rectangle-1_2_").classList.remove("lightbluedark");
                   document.getElementById("Rectangle-1_2_").classList.add("lightbluedark");
                   
                   inputIcono.innerHTML = "";
                   inputIcono.innerHTML = mastercard_single;
                }
                
                if(numeroTarjeta == ""){
                    if (document.getElementById("VISA").hasAttribute("disabled")) {
                    document.getElementById("VISA").removeAttribute("disabled");
                    

                    }
                    if (document.getElementById("MASTERCARD").hasAttribute("disabled")) {
                    document.getElementById("MASTERCARD").removeAttribute("disabled");
               
                    }
                    document.getElementById("VISA").checked = false;
                    document.getElementById("MASTERCARD").checked = false;
                    document.getElementById("cardnumber").value = "";
                    
                    inputIcono.innerHTML = "";
                }
           }
           
           function validarFormulario(){
               
              
               var arrayComprobacion = new Array();
                
                var stringRexp = "[a-zA-Z]+";
               
               //Comprobacion name tarjeta
                var inputName = document.getElementById("name");
                var nameCard = inputName.value;
                
                if(nameCard.match(stringRexp)){
                    inputName.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(nameCard === ""){
                    inputName.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }else{
                    inputName.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                 //Comprobacion tarjeta
                var reVisa = "^4[0-9]{12}(?:[0-9]{3})?$";
                var reMasterCard = "^5[1-5][0-9]{14}$";
                var inputCreditCard = document.getElementById("cardnumber");
                var crediccard = inputCreditCard.value;
                var numeroTarjeta = crediccard.replace(/ /g, "");
                
                
                if(numeroTarjeta.match(reVisa)){
                   inputCreditCard.style.borderColor = "green";
                   arrayComprobacion.push(true);
                }else if(numeroTarjeta.match(reMasterCard)){
                    inputCreditCard.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(numeroTarjeta === ""){
                    inputCreditCard.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                else{
                    inputCreditCard.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                //Comprobación expirate date
                var dateRexp = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])";
                var inputDate = document.getElementById("expirationdate");
                var date = inputDate.value;
                
                if(date.match(dateRexp)){
                    inputDate.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else{
                    inputDate.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                 //Comprobación codigo de seguridad
                var cvvRexp = "[0-9]{3}";
                var inputCVV = document.getElementById("securitycode");
                var cvv = inputCVV.value;
                
                if(cvv.match(cvvRexp)){
                    inputCVV.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(cvv === ""){
                    inputCVV.style.borderColor = "red";
                    
                    arrayComprobacion.push(false);
                }else{
                    inputCVV.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                
                var contador=0;
                for(var i=0; i<arrayComprobacion.length;i++){
                    if(arrayComprobacion[i] === true){
                        contador++;
                    }
                }
                
                if(contador === arrayComprobacion.length){
                    return true;
                }else{
                    return false;
                }
            }
           
           
        </script>

</html>

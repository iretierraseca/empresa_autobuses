<%-- 
    Document   : vistaBillete
    Created on : 02-mar-2019, 2:04:49
    Author     : Irene
--%>

<%@page import="POJO.Ocupacion"%>
<%@page import="POJO.Viajero"%>
<%@page import="java.util.Set"%>
<%@page import="POJO.Reserva"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design Bootstrap</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.7.0.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/billete.css" rel="stylesheet">
        <link href="../css/estilosImpresion.css" rel="stylesheet">
        

    </head>
    <body>
        
         <%
                //HttpSession SesionUsuario = request.getSession(true);
                Reserva objReserva = (Reserva) session.getAttribute("objReserva");
                
             objReserva.getLocalizadorReserva();
                
                Set<Ocupacion> arrayOcupacion = (Set<Ocupacion>)objReserva.getOcupacions();
                
                for(Ocupacion objOcupacion:arrayOcupacion){
                objOcupacion.getViajero().getApellidosViajero();
                }
                
               
            %>

        <main class="mt-5">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
                <div class="container">
                    <a class="navbar-brand" href="#"><strong>TerraBus</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="container" id="ticket" >
                <%for(Ocupacion objOcupacion:arrayOcupacion){%>

                <div class="main-content">
                    <div class="ticket" >
                        <div class="ticket__main">
                            <div class="header"><%=objReserva.getViaje().getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta().getLocalidadEstacion()%>-<%=objReserva.getViaje().getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta().getLocalidadEstacion()%></div>
                            <div class="info passenger">
                                <div class="info__item">Pasajero</div>
                                <div class="info__detail"><%=objOcupacion.getViajero().getNombreViajero()%> <%=objOcupacion.getViajero().getApellidosViajero()%></div>
                            </div>
                            <div class="info platform codigoqr" id="qrcode"> 

                            </div>
                            <div class="info departure">
                                <div class="info__item">Origen</div>
                                <div class="info__detail"><%=objReserva.getViaje().getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta().getLocalidadEstacion()%></div>
                            </div>
                            <div class="info arrival">
                                <div class="info__item">Destino</div>
                                <div class="info__detail"><%=objReserva.getViaje().getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta().getLocalidadEstacion()%></div>
                            </div>
                            <div class="info date">
                                <div class="info__item">Fecha</div>
                                <div class="info__detail"><%=objReserva.getViaje().getFechaViaje()%></div>
                            </div>
                            <div class="info time">
                                <div class="info__item">Hora</div>
                                <div class="info__detail"><%=objReserva.getViaje().getHorario().getHoraSalidaHorario()%></div>
                            </div>
                            <div class="info carriage">
                                <div class="info__item">Precio</div>
                                <div class="info__detail"><%=objReserva.getViaje().getHorario().getRuta().getPrecioRuta()%>€</div>
                            </div>
                            <div class="info seat">
                                <div class="info__item">Asiento</div>
                                <div class="info__detail"><%=objOcupacion.getAsientoOcupacion()%></div>
                            </div>
                            <div class="fineprint">
                                <p>Este tiquet no es transferible • TerraBus S.L</p>
                            </div>
                            <div class="snack">
                                <svg viewBox="0 -11 414.00053 414">
                                <path d="m202.480469 352.128906c0-21.796875-17.671875-39.46875-39.46875-39.46875-21.800781 0-39.472657 17.667969-39.472657 39.46875 0 21.800782 17.671876 39.472656 39.472657 39.472656 21.785156-.023437 39.445312-17.683593 39.46875-39.472656zm0 0"></path>
                                <path d="m348.445312 348.242188c2.148438 21.691406-13.695312 41.019531-35.390624 43.167968-21.691407 2.148438-41.015626-13.699218-43.164063-35.390625-2.148437-21.691406 13.695313-41.019531 35.386719-43.167969 21.691406-2.148437 41.019531 13.699219 43.167968 35.390626zm0 0"></path>
                                <path d="m412.699219 63.554688c-1.3125-1.84375-3.433594-2.941407-5.699219-2.941407h-311.386719l-3.914062-24.742187c-3.191407-20.703125-21.050781-35.9531252-42-35.871094h-42.699219c-3.867188 0-7 3.132812-7 7s3.132812 7 7 7h42.699219c14.050781-.054688 26.03125 10.175781 28.171875 24.0625l33.800781 213.515625c3.191406 20.703125 21.050781 35.957031 42 35.871094h208.929687c3.863282 0 7-3.132813 7-7 0-3.863281-3.136718-7-7-7h-208.929687c-14.050781.054687-26.03125-10.175781-28.171875-24.0625l-5.746094-36.300781h213.980469c18.117187-.007813 34.242187-11.484376 40.179687-28.597657l39.699219-114.578125c.742188-2.140625.402344-4.511718-.914062-6.355468zm0 0"></path>
                                </svg>
                            </div>
                            <div class="barcode">
                                <div class="barcode__scan"></div>
                                <div class="barcode__id"><%=objReserva.getLocalizadorReserva()%></div>
                            </div>
                        </div>
                        <div class="ticket__side">
                            <div class="logo">
                                <p></p>
                            </div>
                            <div class="info side-arrive">
                                <div class="info__item">Origen</div>
                                <div class="info__detail"><%=objReserva.getViaje().getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta().getLocalidadEstacion()%></div>
                            </div>
                            <div class="info side-depart">
                                <div class="info__item">Destino</div>
                                <div class="info__detail"><%=objReserva.getViaje().getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta().getLocalidadEstacion()%></div>
                            </div>
                            <div class="info side-date">
                                <div class="info__item">Fecha</div>
                                <div class="info__detail"><%=objReserva.getViaje().getFechaViaje()%></div>
                            </div>
                            <div class="info side-time">
                                <div class="info__item">Hora</div>
                                <div class="info__detail"><%=objReserva.getViaje().getHorario().getHoraSalidaHorario()%></div>
                            </div>
                            <div class="barcode">
                                <div class="barcode__scan"></div>
                                <div class="barcode__id"><%=objReserva.getLocalizadorReserva()%></div>
                            </div>
                        </div>
                    </div>
                </div>
                <%}%>
                </div>
                <div class="container text-center mt-5"> 
                    <button class="btn blue-gradient botonImprimir" onclick="imprimir()">Imprimir</button>
                </div>
            
            
        </main>

                <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/qrcode_js@1/qrcode.min.js"></script>
          <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js'></script>
  <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js'></script>
        <script>//<![CDATA[
            var input = document.getElementsByClassName("codigoqr");
            for(var i=0;i<input.length;i++){
            var qrcode = new QRCode(document.getElementsByClassName("codigoqr")[i], {//]]>
                text: "TerraBus", //<![CDATA[
                width: 117,
                height: 120,
                colorDark: "#000000",
                colorLight: "rgba(52,179,247,1)",
                correctLevel: QRCode.CorrectLevel.L
            });}
        
            function imprimir() {
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    var objeto=document.getElementById('ticket'); //obtenemos el objeto a imprimir
    document.getElementsByClassName("navbar")[0].style.visibility = "hidden";
    document.getElementsByClassName("step")[0].style.visibility = "hidden";
    document.getElementsByClassName("botonImprimir")[0].style.visibility = "hidden";
    
    window.print();
    document.getElementsByClassName("navbar")[0].style.visibility = "visible";
    document.getElementsByClassName("step")[0].style.visibility = "visible";
    document.getElementsByClassName("botonImprimir")[0].style.visibility = "visible";

}
            //]]></script>

    </body>
</html>

<%-- 
    Document   : vistaResumenCompra
    Created on : 31-ene-2019, 14:48:01
    Author     : Irene
--%>


<%@page import="MODELO.ViajeroAsiento"%>
<%@page import="POJO.Viaje"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design Bootstrap</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.6.1.min.css">
        <link rel="stylesheet" href="../css/compiled-4.7.0.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
        <style>

            #contenedorFondo{
                background-image: url('../img/prueba.jpg');
                background-size:100% 100%;
            }

            #contenedorViajes{
                border: solid;
                background: white !important;
                -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            }


            .viajes{
                border:solid;     
            }

        </style>
    </head>

    <body>
        <%
            
            Viaje objViaje = (Viaje) session.getAttribute("viajeElegido");
            List<ViajeroAsiento> arrayViajeroAsiento = (List<ViajeroAsiento>) session.getAttribute("viajerosAsiento");

        %>
        <!-- Main navigation -->
        <main class="mt-5">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
                <div class="container">
                    <a class="navbar-brand" href="#"><strong>TerraBus</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <!-- Horizontal Steppers -->
                <div class="row">
                    <div class="col-md-12">

                        <!-- Stepers Wrapper -->
                        <ul class="stepper stepper-horizontal">

                            <!-- First Step -->
                            <li class="completed">
                                <a href="#!">
                                    <span class="circle">1</span>
                                    <span class="label">Elige tu horario</span>
                                </a>
                            </li>

                            <!-- Second Step -->
                            <li class="active">
                                <a href="#!">
                                    <span class="circle">2</span>
                                    <span class="label">Personalizar asientos</span>
                                </a>
                            </li>
                            <li class="active" style="background-color: rgba(0,0,0,0.2);">
                                <a href="#!">
                                    <span class="circle">3</span>
                                    <span class="label">Resumen compra</span>
                                </a>
                            </li>

                            <!-- Third Step -->
                            <li class="active">
                                <a href="#!">
                                    <span class="circle">4</span>
                                    <span class="label">Finalizar compra</span>
                                </a>
                            </li>

                        </ul>
                        <!-- /.Stepers Wrapper -->

                    </div>
                </div>
                <!-- /.Horizontal Steppers -->
                <div class="row py-5">
                    <div class="col-md-12 text-center viajes pl-0 pr-0 aqua-gradient">
                        <div class="container">
                            <div class="card ">
                                <div class="card-header">
                                    <h4>Resumen compra</h4>
                                </div>
                                <div class="card-body rgba-grey-light">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="font-weight-bold">Origen - Destino</label>
                                            <p><%=objViaje.getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta().getLocalidadEstacion()%> - <%= objViaje.getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta().getLocalidadEstacion()%></p>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="font-weight-bold">Fecha Salida</label>
                                            <p><%=objViaje.getFechaViaje()%></p>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="font-weight-bold">Hora Salida</label>
                                            <p><%=objViaje.getHorario().getHoraSalidaHorario()%></p>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="font-weight-bold">Precio</label>
                                            <p><%=objViaje.getHorario().getRuta().getPrecioRuta()%></p>
                                        </div>
                                    </div>
                                    <hr>
                                    <!--bucle-->
                                    <%for (ViajeroAsiento objViajero : arrayViajeroAsiento) {%>
                                    <div class="row">
                                        <div class="col-md-5" style="text-align: left">
                                            <h4>Viajero</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="font-weight-bold"><%=objViajero.getTipoIdentificadorViajero()%></label>
                                            <p><%=objViajero.getIdentificadorViajero()%></p>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="font-weight-bold">Nombre y apellidos</label>
                                            <p><%=objViajero.getNombreViajero()%> <%=objViajero.getApellidosViajero()%></p>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="font-weight-bold">Asiento</label>
                                            <p><%=objViajero.getAsiento()%></p>
                                        </div>
                                    </div>
                                    <hr>
                                    <%}%>
                                    <form action="vistaLoginCliente.jsp">
                                        <button type="submit" class="btn" name="botonSiguiente">Login</button>
                                    </form>
                                    <form action="vistaTarjetaRegistro.jsp">
                                        <button type="submit" class="btn" name="botonSiguiente">Registro</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </main>
        <!-- /Start your project here-->
        

        <!-- SCRIPTS -->
        <script>


        </script>
        <!-- JQuery -->
        <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="../js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="../js/mdb.min.js"></script>

        <script type='text/javascript' src='../js/compiled.0.min.js'></script>
</html>

<%-- 
    Document   : vistaTarjetaRegistro
    Created on : 21-feb-2019, 12:16:51
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design Bootstrap</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.7.0.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/tarjeta.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css"/>

    </head>
    <body>
        <main class="mt-5">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
                <div class="container">
                    <a class="navbar-brand" href="#"><strong>TerraBus</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <!-- Horizontal Steppers -->
                <div class="row">
                    <div class="col-md-12">

                        <!-- Stepers Wrapper -->
                        <ul class="stepper stepper-horizontal">

                            <!-- First Step -->
                            <li class="completed">
                                <a href="#!">
                                    <span class="circle">1</span>
                                    <span class="label">Elige tu horario</span>
                                </a>
                            </li>

                            <!-- Second Step -->
                            <li class="active">
                                <a href="#!">
                                    <span class="circle">2</span>
                                    <span class="label">Personalizar asientos</span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#!">
                                    <span class="circle">3</span>
                                    <span class="label">Resumen compra</span>
                                </a>
                            </li>

                            <!-- Third Step -->
                            <li class="active" style="background-color: rgba(0,0,0,0.2);">
                                <a href="#!">
                                    <span class="circle">4</span>
                                    <span class="label">Finalizar compra</span>
                                </a>
                            </li>

                        </ul>
                        <!-- /.Stepers Wrapper -->

                    </div>
                </div>

            </div>

            <div class="container-fluid">

                <form action="../controladorPagarRegistro" id="formularioTarjetaR" onsubmit="return validarFormulario();" >
                    <div class="payment-title">
                        <h1>Información de pago</h1>
                    </div>
                    <div class="container-fluid d-inline-flex">
                        <div class="containerTarjeta preload">
                            <div class="creditcard">
                                <div class="front">
                                    <div id="ccsingle"></div>
                                    <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                    <g id="Front">
                                    <g id="CardBackground">
                                    <g id="Page-1_1_">
                                    <g id="amex_1_">
                                    <path id="Rectangle-1_1_" class="lightcolor grey" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                          C0,17.9,17.9,0,40,0z" />
                                    </g>
                                    </g>
                                    <path class="darkcolor greydark" d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z" />
                                    </g>
                                    <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber" class="st2 st3 st4">0123 4567 8910 1112</text>
                                    <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname" class="st2 st5 st6">JOHN DOE</text>
                                    <text transform="matrix(1 0 0 1 54.1074 389.8793)" class="st7 st5 st8">cardholder name</text>
                                    <text transform="matrix(1 0 0 1 479.7754 388.8793)" class="st7 st5 st8">expiration</text>
                                    <text transform="matrix(1 0 0 1 65.1054 241.5)" class="st7 st5 st8" >card number</text>
                                    <g>
                                    <text transform="matrix(1 0 0 1 574.4219 433.8095)" id="svgexpire" class="st2 st5 st9">01/23</text>
                                    <text transform="matrix(1 0 0 1 479.3848 417.0097)" class="st2 st10 st11">VALID</text>
                                    <text transform="matrix(1 0 0 1 479.3848 435.6762)" class="st2 st10 st11">THRU</text>
                                    <polygon class="st2" points="554.5,421 540.4,414.2 540.4,427.9 		" />
                                    </g>
                                    <g id="cchip">
                                    <g>
                                    <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                                          c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z" />
                                    </g>
                                    <g>
                                    <g>
                                    <rect x="82" y="70" class="st12" width="1.5" height="60" />
                                    </g>
                                    <g>
                                    <rect x="167.4" y="70" class="st12" width="1.5" height="60" />
                                    </g>
                                    <g>
                                    <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                                          c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                                          C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                                          c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                                          c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z" />
                                    </g>
                                    <g>
                                    <rect x="82.8" y="82.1" class="st12" width="25.8" height="1.5" />
                                    </g>
                                    <g>
                                    <rect x="82.8" y="117.9" class="st12" width="26.1" height="1.5" />
                                    </g>
                                    <g>
                                    <rect x="142.4" y="82.1" class="st12" width="25.8" height="1.5" />
                                    </g>
                                    <g>
                                    <rect x="142" y="117.9" class="st12" width="26.2" height="1.5" />
                                    </g>
                                    </g>
                                    </g>
                                    </g>
                                    <g id="Back">
                                    </g>
                                    </svg>
                                </div>
                                <div class="back">
                                    <svg version="1.1" id="cardback" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                    <g id="Front">
                                    <line class="st0" x1="35.3" y1="10.4" x2="36.7" y2="11" />
                                    </g>
                                    <g id="Back">
                                    <g id="Page-1_2_">
                                    <g id="amex_2_">
                                    <path id="Rectangle-1_2_" class="darkcolor greydark" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                          C0,17.9,17.9,0,40,0z" />
                                    </g>
                                    </g>
                                    <rect y="61.6" class="st2" width="750" height="78" />
                                    <g>
                                    <path class="st3" d="M701.1,249.1H48.9c-3.3,0-6-2.7-6-6v-52.5c0-3.3,2.7-6,6-6h652.1c3.3,0,6,2.7,6,6v52.5
                                          C707.1,246.4,704.4,249.1,701.1,249.1z" />
                                    <rect x="42.9" y="198.6" class="st4" width="664.1" height="10.5" />
                                    <rect x="42.9" y="224.5" class="st4" width="664.1" height="10.5" />
                                    <path class="st5" d="M701.1,184.6H618h-8h-10v64.5h10h8h83.1c3.3,0,6-2.7,6-6v-52.5C707.1,187.3,704.4,184.6,701.1,184.6z" />
                                    </g>
                                    <text transform="matrix(1 0 0 1 621.999 227.2734)" id="svgsecurity" class="st6 st7">985</text>
                                    <g class="st8">
                                    <text transform="matrix(1 0 0 1 518.083 280.0879)" class="st9 st6 st10">security code</text>
                                    </g>
                                    <rect x="58.1" y="378.6" class="st11" width="375.5" height="13.5" />
                                    <rect x="58.1" y="405.6" class="st11" width="421.7" height="13.5" />
                                    <text transform="matrix(1 0 0 1 59.5073 228.6099)" id="svgnameback" class="st12 st13">John Doe</text>
                                    </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="form-container">
                            <div class="field-container">
                                <label for="name">Name</label>
                                <input id="name" maxlength="20" type="text" name="name">
                            </div>

                            <div class="field-container">
                            <label for="cardnumber">Card Number</label>
                            <input id="cardnumber" name="cardnumber" type="text" inputmode="numeric" onclick="comprobarTexto(this)">
                            <svg id="ccicon" class="ccicon" width="750" height="471" viewBox="0 0 750 471" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink">

                            </svg>
                            <input type="text" id="tipoCreditCard" name="tipoCreditCard" style="display: none">
                        </div>
                            <div class="field-container">
                                <label for="expirationdate">Expiration (mm/yy)</label>
                                <input id="expirationdate" type="text"  inputmode="numeric" name="expirationdate">
                            </div>
                            <div class="field-container">
                                <label for="securitycode">Security Code</label>
                                <input id="securitycode" name="securitycode" type="text"  inputmode="numeric" maxlength="3">
                            </div>
                        </div>
                        <div style="padding-left: 1.2rem;border-left: 1px solid black;">
                            <div style=" margin-top: 20px;" class="row">
                                <div class="col-md-6">
                                    <label for="exampleForm2">Nombre</label>
                                    <input type="text" name="nombreClienteR" id="nombreClienteR" class="form-control" style="height:56px" >
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleForm2">Apellidos</label>
                                    <input type="text" name="apellidoClienteR" id="apellidoClienteR" class="form-control" style="height:56px">
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="exampleForm2">Identificador</label>
                                    <input type="text" id="identificadorClienteR" name="identificadorClienteR" class="form-control" style="height:56px">
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleForm2">Teléfono</label>
                                    <input type="text" id="telefonoClienteR" name="telefonoClienteR" class="form-control" style="height:56px">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="exampleForm2">Email</label>
                                    <input type="email" id="emailClienteR" name="emailClienteR" class="form-control" style="height:56px">
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleForm2">Password</label>
                                    <input type="password" id="passClienteR" name="passClienteR" class="form-control" style="height:56px">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="container" style="text-align: center;">
                        <button class="btn peach-gradient" id="botonPagar">Pagar</button>
                    </div>
                </form>
            </div>

            

        </main>

        <!-- SCRIPTS -->
        
        <script>
            
            
            
            function comprobarTexto(event) {
                event.value = "";
                document.getElementById("name").value = "";
                document.getElementById("expirationdate").value = "";
                var tipoTarjetas = document.getElementsByClassName("tipoTarjeta");
                
                
                for (var i = 0; i < tipoTarjetas.length; i++) {
                    
                    if (tipoTarjetas[i].hasAttribute("checked")) {
                        tipoTarjetas[i].removeAttribute("checked");
                    }
                    if (tipoTarjetas[i].hasAttribute("disabled")) {
                        tipoTarjetas[i].removeAttribute("disabled");
                    }
                    
                }
            }
            
            
        </script>



        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/imask/3.4.0/imask.min.js'></script>
        <script type='text/javascript' src='../js/tarjeta.js'></script>
        <script type='text/javascript' src='../js/ajaxTarjetaCliente.js'></script>

        <!-- JQuery -->
        
        
        <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        
            <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="../js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
          
           
        
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="../js/mdb.min.js"></script>

        <script type='text/javascript' src='../js/compiled.0.min.js'></script>
        
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
        
      
        <script>
            
            
            
            function validarFormulario(){
                
                var tarjetaTipo = document.getElementsByName("tipo")[0].id;
                
                document.getElementById("tipoCreditCard").value = tarjetaTipo;
                
                var formulario = document.getElementById("formularioTarjetaR");
                
                var arrayComprobacion = new Array();
                
                var stringRexp = "[a-zA-Z]+";
                
                //Comprobacion name tarjeta
                var inputName = document.getElementById("name");
                var nameCard = inputName.value;
                
                if(nameCard.match(stringRexp)){
                    inputName.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(nameCard === ""){
                    inputName.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }else{
                    inputName.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
               
                //Comprobacion tarjeta
                var reVisa = "^4[0-9]{12}(?:[0-9]{3})?$";
                var reMasterCard = "^5[1-5][0-9]{14}$";
                var inputCreditCard = document.getElementById("cardnumber");
                var crediccard = inputCreditCard.value;
                var numeroTarjeta = crediccard.replace(/ /g, "");
                
                
                if(numeroTarjeta.match(reVisa)){
                   //alert("Tarjeta visa"); 
                   
                   //document.getElementById("tipoCreditCard").value = tipoCard;
                   inputCreditCard.style.borderColor = "green";
                   arrayComprobacion.push(true);
                }else if(numeroTarjeta.match(reMasterCard)){
                    //alert("Tarjeta mastercard");
                    inputCreditCard.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(numeroTarjeta === ""){
                    inputCreditCard.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                else{
                    inputCreditCard.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                //Comprobación expirate date
                var dateRexp = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])";
                var inputDate = document.getElementById("expirationdate");
                var date = inputDate.value;
                
                if(date.match(dateRexp)){
                    inputDate.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else{
                    inputDate.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                //Comprobación codigo de seguridad
                var cvvRexp = "[0-9]{3}";
                var inputCVV = document.getElementById("securitycode");
                var cvv = inputCVV.value;
                
                if(cvv.match(cvvRexp)){
                    inputCVV.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(cvv === ""){
                    inputCVV.style.borderColor = "red";
                    
                    arrayComprobacion.push(false);
                }else{
                    inputCVV.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                //Comprobación nombre
                var inputNombre = document.getElementById("nombreClienteR");
                var nombre = inputNombre.value;
                
                if(nombre.match(stringRexp)){
                    inputNombre.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(nombre === ""){
                    inputNombre.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }else{
                    inputNombre.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                //Comprobación apellidos
                var inputApellidos = document.getElementById("apellidoClienteR");
                var apellidos = inputApellidos.value;
                
                if(apellidos.match(stringRexp)){
                    inputApellidos.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(apellidos === ""){
                    inputApellidos.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }else{
                    inputApellidos.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                //Comprobacion telefono 
                
                var telefonoRexp = "[0-9]{9}";
                var inputTelefono = document.getElementById("telefonoClienteR");
                var telefono = inputTelefono.value;
                
                if(telefono.match(telefonoRexp)){
                    inputTelefono.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(telefono === ""){
                    inputTelefono.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }else{
                    inputTelefono.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                //Comprobacion email
                
                var emailRexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$";
                var inputEmail = document.getElementById("emailClienteR");
                var email = inputEmail.value;
                
                if(email.match(emailRexp)){
                    inputEmail.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(email === ""){
                    inputEmail.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }else{
                    inputEmail.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
                //Comprobacion password
                
                var inputPass = document.getElementById("passClienteR");
                var pass = inputPass.value;
                
                if(pass === ""){
                    inputPass.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }else{
                    inputPass.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }
                
                //Comprobación nif, nie
                var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
                var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
                
                var inputdni = document.getElementById("identificadorClienteR");
                var dni = inputdni.value;
                
                if(dni.match(nifRexp)){
                    inputdni.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(dni.match(nieRexp)){
                    inputdni.style.borderColor = "green";
                    arrayComprobacion.push(true);
                }else if(dni === ""){
                    inputdni.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }else{
                    inputdni.style.borderColor = "red";
                    arrayComprobacion.push(false);
                }
                
        
                var contador=0;
                for(var i=0; i<arrayComprobacion.length;i++){
                    if(arrayComprobacion[i] === true){
                        contador++;
                    }
                }
                
                if(contador === arrayComprobacion.length){
                    return true;
                }else{
                    return false;
                }
        
               
           
                
            }
            
            
            
            
        </script>
        
       
        
        
    
    </body>
</html>

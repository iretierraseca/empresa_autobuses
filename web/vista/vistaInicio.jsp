<%-- 
    Document   : vistaInicio
    Created on : 15-ene-2019, 0:48:26
    Author     : Irene
--%>

<%@page import="java.util.List"%>
<%@page import="POJO.Ruta"%>
<%@page import="java.util.Iterator"%>
<%@page import="POJO.Estacion"%>
<%@page import="java.util.Set"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design Bootstrap</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.6.1.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>

        <!-- Start your project here-->
        <!-- Main navigation -->
        <!-- Main navigation -->
        <header>

            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
                <div class="container">
                    <a class="navbar-brand" href="#"><strong>TerraBus</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="vistaLoginAdministrador.jsp">Viajar <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- Navbar -->
            <!-- Full Page Intro -->
            <div class="view jarallax" data-jarallax='{"speed": 0.2}'>
                <video id="video" autoplay preload muted><!--Para reproducir continuamente: añadir loop-->
                    <source src="../img/video.mp4" type='video/mp4' />
                </video>
                <!-- Mask & flexbox options-->
                <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
                    <!-- Content -->
                    <div class="container">
                        <!--Grid row-->
                        <div class="row mt-5">
                            <!--Grid column-->
                            <div class="col-md-6 mb-5 mt-md-0 mt-5 white-text text-center text-md-left">
                                <h1 class="h1-responsive font-weight-bold wow fadeInLeft" data-wow-delay="0.3s">TerraBus</h1>
                                <hr class="hr-medium wow fadeInLeft" data-wow-delay="0.3s">
                                <h2 class="mb-3 wow fadeInLeft" data-wow-delay="0.3s">Tan lejos como te imagines, tan cerca como quieras estar</h2>
                            </div>
                            <!--Grid column-->
                            <div class="col-md-6 col-xl-5 mb-4">
                                <!--Form-->
                                <div class="card wow fadeInRight" data-wow-delay="0.3s">
                                    <div class="card-body">
                                        <form action="../controladorOfertaHorariaTrayecto">
                                            <div class="md-form input-group">
                                                <%
                                                    //HttpSession SesionUsuario = request.getSession(true);
                                                    Set<Estacion> setEstaciones = (Set<Estacion>) session.getAttribute("listaOrigen");

                                                %>
                                                <div>
                                                    <select required class="mdb-select md-form mb-4 initialized" id="selectOrigen" name="selectOrigen" onChange="muestradestinos(this.value)">
                                                        <option value="" disabled selected >Origen</option>
                                                        <%

                                                           for(Estacion item: setEstaciones){
                    
                
                                                        %>
                                                        <option value="<%=item.getIdEstacion()%>"><%=item.getLocalidadEstacion()%></option>
                                                        <%}%>

                                                    </select>
                                                </div>
                                                <div>
                                                    <select required class="mdb-select md-form mb-4 initialized" id="selectDestino" name="selectDestino">
                                                        <option value="" disabled selected>Destino</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--Body-->
                                            <label for="fechaViaje" class="white-text">Fecha</label>
                                            <input required type="date" name="fechaViaje" id="fechaViaje" class="form-control mb-4" min="2019-03-22" max="2019-03-23">

                                            <label for="numeroPasajeros" class="white-text">Número pasajeros</label>
                                            <input required type="number" name="numeroPasajeros" id="numeroPasajeros" class="form-control mb-4" placeholder="Text input" min="1" max="8">

                                            <div class="text-center mt-4">
                                                <button class="btn aqua-gradient">Buscar</button>
                                                <hr class="hr-medium mb-3 mt-4">
                                                <div class="inline-ul text-center d-flex justify-content-center">
                                                    <a class="p-2 m-2 tw-ic">
                                                        <i class="fab fa-twitter white-text"></i>
                                                    </a>
                                                    <a class="p-2 m-2 li-ic">
                                                        <i class="fab fa-facebook-f white-text"></i>
                                                    </a>
                                                    <a class="p-2 m-2 ins-ic">
                                                        <i class="fab fa-instagram white-text"> </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!--/.Form-->
                            </div>
                            <!--Grid column-->
                        </div>
                        <!--Grid row-->
                    </div>
                    <!-- Content -->
                </div>
                <!-- Mask & flexbox options-->

            </div>
            <!-- Full Page Intro -->
        </header>
        <!-- Main navigation -->

        <!-- /Start your project here-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="../js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="../js/mdb.min.js"></script>

        <script type='text/javascript' src='../js/compiled.0.min.js'></script>
        <script>
                                                        new WOW().init();
                                                        $(document).ready(function () {
                                                            $('.mdb-select').materialSelect();
                                                        });
        </script>

        <script>
            function muestradestinos(str) {
                var xmlhttp = new XMLHttpRequest();

                if (str == "null") {
                    document.getElementById("selectDestino").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    /*Como el explorer va por su cuenta, el objeto es un ActiveX */
                }

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById("selectDestino").innerHTML = xmlhttp.responseText; /*Seleccionamos el elemento que recibirá el flujo de datos*/
                    }
                }
                xmlhttp.open("GET", "../controladorAjaxDestinoRuta?origen=" + str, true);

                xmlhttp.send();


            }// muestraDestinos
        </script>
    </body>

</html>

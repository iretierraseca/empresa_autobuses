<%-- 
    Document   : vistaLoginAdministrador
    Created on : 04-mar-2019, 2:45:07
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design Bootstrap</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.6.1.min.css">
        <link rel="stylesheet" href="../css/compiled-4.7.0.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
        <style>

            #contenedorFondo{
                background-image: url('../img/prueba.jpg');
                background-size:100% 100%;
            }

            #contenedorViajes{
                border: solid;
                background: white !important;
                -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            }


            .viajes{
                border:solid;     
            }

            .form-gradient .font-small {
                font-size: 0.8rem; }

            .form-gradient .header {
                border-top-left-radius: .3rem;
                border-top-right-radius: .3rem; }

            .form-gradient input[type=text]:focus:not([readonly]) {
                border-bottom: 1px solid #fd9267;
                -webkit-box-shadow: 0 1px 0 0 #fd9267;
                box-shadow: 0 1px 0 0 #fd9267; }

            .form-gradient input[type=text]:focus:not([readonly]) + label {
                color: #4f4f4f; }

            .form-gradient input[type=password]:focus:not([readonly]) {
                border-bottom: 1px solid #fd9267;
                -webkit-box-shadow: 0 1px 0 0 #fd9267;
                box-shadow: 0 1px 0 0 #fd9267; }

            .form-gradient input[type=password]:focus:not([readonly]) + label {
                color: #4f4f4f; }

        </style>
    </head>

    <body>
        <%


        %>
        <!-- Main navigation -->
        <main class="mt-5">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
                <div class="container">
                    <a class="navbar-brand" href="#"><strong>TerraBus</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="container">
            <!--Section: Live preview-->
            <section class="form-light" style="width:500px;margin:5rem;margin: 0 auto;margin-top: 5rem;margin-bottom: 5rem;">

                <!--Form without header-->
                <div class="card">

                    <div class="card-body mx-4">
                        <form action="../controladorLoginAdministrador">

                        <!--Header-->
                        <div class="text-center">
                            <h3 class="teal-text mb-5"><strong>Sign up</strong></h3>
                        </div>

                        <!--Body-->
                            <input type="text" id="Form-email2" class="form-control" name="nombreAdmin">
                            <label for="Form-email2">Usuario</label>
                        

                            <input type="password" id="Form-pass2" class="form-control" name="passAdmin">
                            <label for="Form-pass2">Contraseña</label>
                        

                        <!--Grid row-->
                        <div class="row d-flex align-items-center mb-4 mt-4">

                            <!--Grid column-->
                            <div class="col-md-3 col-md-6 text-center">
                                <button type="submit" class="btn aqua-gradient btn-block btn-rounded z-depth-1">Sign up</button>
                            </div>
                            <!--Grid column-->

                            <!--Grid column-->
                            
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->
                        </form>
                    </div>

                    <!--Footer-->
                    <div class="footer pt-3 aqua-gradient lighten-3">

                        <div class="row d-flex justify-content-center">
                            <p class="font-small white-text mb-2 pt-3">or Sign up with:</p>
                        </div>

                        <div class="row mt-2 mb-3 d-flex justify-content-center">
                            <!--Facebook-->
                            <a class="fa-lg p-2 m-2 fb-ic"><i class="fab fa-facebook-f white-text fa-lg"> </i></a>
                            <!--Twitter-->
                            <a class="fa-lg p-2 m-2 tw-ic"><i class="fab fa-twitter white-text fa-lg"> </i></a>
                            <!--Google +-->
                            <a class="fa-lg p-2 m-2 gplus-ic"><i class="fab fa-google-plus-g white-text fa-lg"> </i></a>
                        </div>

                    </div>

                </div>
                <!--/Form without header-->

            </section>
            <!--Section: Live preview-->
            </div>
        </main>
        <!-- /Start your project here-->


        <!-- SCRIPTS -->
        <script>


        </script>
        <!-- JQuery -->
        <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="../js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="../js/mdb.min.js"></script>

        <script type='text/javascript' src='../js/compiled.0.min.js'></script>
</html>

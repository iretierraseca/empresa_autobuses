<%-- 
    Document   : vistaViajeros
    Created on : 29-ene-2019, 22:35:40
    Author     : Irene
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design Bootstrap</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.6.1.min.css">
        <link rel="stylesheet" href="../css/compiled-4.7.0.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
        <style>

            #contenedorFondo{
                background-image: url('../img/prueba.jpg');
                background-size:100% 100%;
            }

            #contenedorViajes{
                border: solid;
                background: white !important;
                -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            }




        </style>
    </head>

    <body>
        <%
            //HttpSession SesionUsuario = request.getSession(true);
            int numeroPasajeros = (Integer) session.getAttribute("numeroPasajeros");
            int plazasAutobus = (Integer) session.getAttribute("plazasAutobus");
            List<Integer> plazasAutobusOcupadas = (List<Integer>) session.getAttribute("asientosOcupados");

        %>
        <!-- Main navigation -->
        <main class="mt-5">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
                <div class="container">
                    <a class="navbar-brand" href="#"><strong>TerraBus</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <!-- Horizontal Steppers -->
                <div class="row">
                    <div class="col-md-12">

                        <!-- Stepers Wrapper -->
                        <ul class="stepper stepper-horizontal">

                            <!-- First Step -->
                            <li class="completed">
                                <a href="#!">
                                    <span class="circle">1</span>
                                    <span class="label">Elige tu horario</span>
                                </a>
                            </li>

                            <!-- Second Step -->
                            <li class="active" style="background-color: rgba(0,0,0,0.2);">
                                <a href="#!">
                                    <span class="circle">2</span>
                                    <span class="label">Personalizar asientos</span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#!">
                                    <span class="circle">3</span>
                                    <span class="label">Resumen compra</span>
                                </a>
                            </li>

                            <!-- Third Step -->
                            <li class="active">
                                <a href="#!">
                                    <span class="circle">4</span>
                                    <span class="label">Finalizar compra</span>
                                </a>
                            </li>

                        </ul>
                        <!-- /.Stepers Wrapper -->

                    </div>
                </div>
                <!-- /.Horizontal Steppers -->
                <div class="row py-5">
                    <div class="col-md-12 text-center viajes pl-0 pr-0">
                        <div class="container">
                            <!-- Grid row -->
                            <div class="row aqua-gradient d-flex justify-content-center">

                                <!-- Grid column -->
                                <div class="col-md-10 col-xl-10 py-5">

                                    <!--Accordion wrapper-->
                                    <div class="accordion md-accordion accordion-2" id="accordionEx7" role="tablist" aria-multiselectable="true">   

                                        <form action="../controladorResumenCompra" onsubmit="return enviar(<%=numeroPasajeros%>);">
                                            <%for (int i = 1; i <= numeroPasajeros; i++) {%>
                                            <!-- Accordion card -->
                                            <div class="card viajeros">

                                                <!-- Card header -->
                                                <div class="card-header rgba-stylish-strong z-depth-1 mb-1" role="tab" id="heading<%=i%>">
                                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx7" href="#collapse<%=i%>"
                                                       aria-expanded="false" aria-controls="collapse<%=i%>">
                                                        <h5 class="mb-0 white-text  font-thin">
                                                            Datos del viajero <%=i%> <i class="fas fa-angle-down rotate-icon"></i>
                                                        </h5>
                                                    </a>
                                                </div>

                                                <!-- Card body -->
                                                <div id="collapse<%=i%>" class="collapse" role="tabpanel" aria-labelledby="heading<%=i%>" data-parent="#accordionEx7">
                                                    <div class="card-body mb-1 rgba-grey-light white-text">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label for="exampleForm2">Identificador</label>
                                                                <input type="text" aria-label="Identificador" class="form-control" placeholder="Numero" name="numIdentificadorViajero<%=i%>">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="exampleForm2">Nombre</label>
                                                                <input type="text" aria-label="First name" class="form-control" placeholder="Nombre" name="nombreViajero<%=i%>"> 
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="exampleForm2">Apellidos</label>
                                                                <input type="text" aria-label="Last name" class="form-control" placeholder="Apellidos" name="apellidosViajero<%=i%>">
                                                            </div>
                                                            <div class="col-md-7" style="margin:0 auto;">
                                                                    <label for="exampleForm2">NºAsiento</label>

                                                                    <input id="asientoViajero<%=i%>" type="number" aria-label="numAsiento" class="form-control asientosViajeros" onclick="modal(this)" name="asientosViajero<%=i%>">
                                                                
                                                            </div>
                                                            <div class="col-md-7" style="margin:0 auto;">

                                                                <input id="tipoIdentificador<%=i%>" type="text" class="form-control"  name="tipoIdentificador<%=i%>" style="display:none">
                                                                
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <%}%>
                                            <button class="btn" name="botonSiguiente">Siguiente</button>

                                            <!-- Accordion card -->
                                        </form>
                                    </div>
                                    <!--/.Accordion wrapper-->
                                </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row -->
                        </div>
                    </div>
                </div>


                <!-- Modal -->
                <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content d-flex align-items-center">
                            <div class="modal-header w-100">
                                <h5 class="modal-title" id="exampleModalLabel">Asientos bus</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body d-flex col-7 flex-wrap justify-content-center">

                                <%
                                    for (int i = 1; i <= plazasAutobus; i++) {%>
                                <%for (int item : plazasAutobusOcupadas) {
                                        if (i == item) {

                                %>

                                <button type="button" class="btn btn-danger asientoBus" disabled><%=i%></button>

                                <% i++;
                                    } %>

                                <%}%>
                                <button type="button" class="btn btn-light asientoBus"><%=i%></button>
                                <%}%>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="guardarAsiento()">Elegir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- /Start your project here-->

        <!-- SCRIPTS -->
        <script>
            

            var inputModal;

            function modal(event) {
                $('#basicExampleModal').modal('show')
                inputModal = event;
                asientos();
            }

            function asientos() {
                var asientos = document.getElementsByClassName("asientoBus");
                for (var i = 0; i < asientos.length; i++) {
                    asientos[i].addEventListener("click", function () {
                        cambiarAsiento(this);
                    });
                }
            }

            function cambiarAsiento(boton) {
                var asientosV = document.getElementsByClassName("asientoBus btn-default");

                for (var i = 0; i < asientosV.length; i++) {
                    asientosV[i].classList.toggle("btn-light");
                    asientosV[i].classList.toggle("btn-default");
                }

                boton.classList.toggle("btn-light");
                boton.classList.toggle("btn-default");


            }

            function guardarAsiento() {

                var asientosG = document.getElementsByClassName("asientoBus btn-default");
                inputModal.value = asientosG[0].textContent;
                asientosG[0].classList.add("btn-warning");
                asientosG[0].setAttribute("disabled", "");
                inputModal.setAttribute("disabled", "");
                asientosG[0].classList.remove("btn-default");

            }

            function enviar(numPasajeros) {
               
                var inputAsientos = document.getElementsByClassName("asientosViajeros");
                for (var i = 0; i < inputAsientos.length; i++) {
                    inputAsientos[i].removeAttribute("disabled");
                }
                
                var arrayComprobacion = new Array();
                
                //Comprobación nif, nie
                var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
                var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
                var stringRexp = "[a-zA-Z]+";
                
                for(var i=0;i<numPasajeros;i++){
                    var inputdni = document.getElementsByName("numIdentificadorViajero"+(i+1))[0];
                    var inputtipo = document.getElementsByName("tipoIdentificador"+(i+1))[0];
                    var dni = inputdni.value;
                
                    if(dni.match(nifRexp)){
                        inputdni.style.borderColor = "green";
                        inputtipo.value = "NIF";
                        arrayComprobacion.push(true);
                    }else if(dni.match(nieRexp)){
                        inputdni.style.borderColor = "green";
                        inputtipo.value = "NIE";
                        arrayComprobacion.push(true);
                    }else if(dni === ""){
                        inputdni.style.borderColor = "red";
                        arrayComprobacion.push(false);
                    }else{
                        inputdni.style.borderColor = "red";
                        arrayComprobacion.push(false);
                    }
                    
                    //________________________________________//
                    
                    var inputNombre = document.getElementsByName("nombreViajero"+(i+1))[0];
                    var nombre = inputNombre.value;
                
                    if(nombre.match(stringRexp)){
                        inputNombre.style.borderColor = "green";
                        arrayComprobacion.push(true);
                    }else if(nombre === ""){
                        inputNombre.style.borderColor = "red";
                        arrayComprobacion.push(false);
                    }else{
                        inputNombre.style.borderColor = "red";
                        arrayComprobacion.push(false);
                    }
                    
                    //Comprobación apellidos
                    var inputApellidos = document.getElementsByName("apellidosViajero"+(i+1))[0];
                    var apellidos = inputApellidos.value;
                
                    if(apellidos.match(stringRexp)){
                        inputApellidos.style.borderColor = "green";
                        arrayComprobacion.push(true);
                    }else if(apellidos === ""){
                        inputApellidos.style.borderColor = "red";
                        arrayComprobacion.push(false);
                    }else{
                        inputApellidos.style.borderColor = "red";
                        arrayComprobacion.push(false);
                    }
                    
                    var inputAsiento = document.getElementsByName("asientosViajero"+(i+1))[0];
                    var asiento = inputAsiento.value;
                    
                    if(asiento !== ""){
                        inputAsiento.style.borderColor = "green";
                        arrayComprobacion.push(true);
                    }else{
                        inputAsiento.style.borderColor = "red";
                        arrayComprobacion.push(false);
                    }
                }
                
                var found = arrayComprobacion.find(function(element){
                    return element == false;
                });
               
                if(found !== false){
                    return true;
                }else{
                    return false;
                }
            }

        </script>
        <!-- JQuery -->
        <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="../js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="../js/mdb.min.js"></script>

        <script type='text/javascript' src='../js/compiled.0.min.js'></script>
</html>

<%-- 
    Document   : vistaError
    Created on : 10-mar-2019, 19:17:28
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.6.1.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/estiloErrores.css"/>
    </head>
    <body>
        <main>
            <%String error = request.getParameter("error");%>
            
            <div class="container"><h1 class="white-text">Se ha producido un error</h1><p class="white-text"><%=error%></p></div>
            <div class="stop">
                <div class="face">
                    <svg width="144px" height="144px" viewBox="0 0 144 144">
                    <g id="Page-1">
                    <polygon id="Polygon" stroke="#FFFFFF" stroke-width="4" fill="#DF2626" transform="translate(71.634284, 71.634284) rotate(-112.000000) translate(-71.634284, -71.634284) " points="71.6342836 -3.3657164 124.667292 18.601275 146.634284 71.6342836 124.667292 124.667292 71.6342836 146.634284 18.601275 124.667292 -3.3657164 71.6342836 18.601275 18.601275"></polygon>
                    </g>
                    </svg>

                    <div class="text">STOP</div>
                </div>

                <div class="backface">
                    <svg width="144px" height="144px" viewBox="0 0 144 144">
                    <g id="Page-1">
                    <polygon id="Polygon" stroke="#FFFFFF" stroke-width="4" fill="#DF2626" transform="translate(71.634284, 71.634284) rotate(-112.000000) translate(-71.634284, -71.634284) " points="71.6342836 -3.3657164 124.667292 18.601275 146.634284 71.6342836 124.667292 124.667292 71.6342836 146.634284 18.601275 124.667292 -3.3657164 71.6342836 18.601275 18.601275"></polygon>
                    </g>
                    </svg>
                </div>

                <div class="post">
                    <div class="post-front"></div>
                    <div class="post-back"></div>
                    <div class="post-left"></div>
                    <div class="post-right"></div>
                </div>
            </div>

            <div class="car">
                <img src="https://s3.eu-west-2.amazonaws.com/nelsoncodepen/Car.png" width="303" alt="">
            </div>
        </main>
            <% response.setHeader("Refresh", "5;url=../index.jsp"); %>
    </body>
</html>

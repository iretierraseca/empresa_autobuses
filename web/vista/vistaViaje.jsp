<%-- 
    Document   : vistaInicio
    Created on : 15-ene-2019, 0:48:26
    Author     : Irene
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="POJO.Viaje"%>
<%@page import="POJO.Viaje"%>
<%@page import="java.util.Iterator"%>
<%@page import="POJO.Estacion"%>
<%@page import="java.util.Set"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design Bootstrap</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="../css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/compiled-4.6.1.min.css">
        <link rel="stylesheet" href="../css/compiled-4.7.0.min.css">
        <!-- Your custom styles (optional) -->
        <link href="../css/style.css" rel="stylesheet">
        <style>
            .stepper-horizontal li:not(:last-child)::after {

                background: white !important;
                /* width: ; */
                height: 2px !important;
            }

            .stepper-horizontal li:not(:first-child)::before {

                background: white !important;
                height: 2px !important;
            }

            .chip:active, .z-depth-1 {
                background: white !important;
                opacity: 0.7 !important;

            }

            #contenedorFondo{
                background-image: url('../img/prueba.jpg');
                background-size:100% 100%;
            }

            .mcircle {
                background: white !important;
                width: 1.75rem !important; 
                height: 1.75rem !important;
            }

            .animated {
                -webkit-animation-duration: 2s;
                animation-duration: 2s;
                -webkit-animation-fill-mode: both;
                animation-fill-mode: both;
            }
            @-webkit-keyframes flash {
                0%, 50%, 100% {
                    opacity: 1;
                }
                25%, 75% {
                    opacity: 0;
                }
            }
            @keyframes flash {
                0%, 50%, 100% {
                    opacity: 1;
                }
                25%, 75% {
                    opacity: 0;
                }
            }
            .flash {
                -webkit-animation-name: flash;
                animation-name: flash;
            }


            .verde{
                background: #00c851 !important;
                width: 1.75rem !important; 
                height: 1.75rem !important;
            }

            #contenedorViajes{
                border: solid;
                background: white !important;
                -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            }

            h1{
                width: 14rem;
                margin-top: -2rem !important;
                background: white;
                margin: 0 auto;
            }

            .viajes{
                border:solid;     
            }

            .textCont{
                width: 14rem;
                margin-top: -1rem !important;
                background: white;
            }






            .clock {
                border: 5px solid black;
                position: relative;
                border-radius: 100%;
                display: block;
                height: 250px;
                width: 250px;


            }
            .hour  {

                margin: -60px -2px 0;
                padding: 60px 2px 0;
                background: black;
                height: 0;
                left: 50%;
                position: absolute;
                top: 50%;
                width: 0;

                -webkit-transform-origin: 50% 100%;
                -moz-transform-origin: 50% 100%;
                -o-transform-origin: 50% 100%;
                -ms-transform-origin: 50% 100%;
                transform-origin: 50% 100%;
            }

            .minute {

                margin: -105px -2px 0;
                padding: 105px 2px 0;
                background: black;
                height: 0;
                left: 50%;
                position: absolute;
                top: 50%;
                width: 0;

                -webkit-transform-origin: 50% 100%;
                -moz-transform-origin: 50% 100%;
                -o-transform-origin: 50% 100%;
                -ms-transform-origin: 50% 100%;
                transform-origin: 50% 100%;
            }


        </style>
    </head>

    <body>

        <!-- Start your project here-->
        <!-- Main navigation -->
        <!-- Main navigation -->
        <header>
            <%
                //HttpSession SesionUsuario = request.getSession(true);
                List<Viaje> arrayViajes = (List<Viaje>) session.getAttribute("viajes");
                int contador = 0;
            %>



            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
                <div class="container">
                    <a class="navbar-brand" href="#"><strong>TerraBus</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- Navbar -->
            <!-- Full Page Intro -->
            <div class="view jarallax" data-jarallax='{"speed": 0.2}' id="contenedorFondo">

                <!-- Mask & flexbox options-->
                <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
                    <!-- Content -->
                    <div class="container">



                        <!-- Timeline -->
                        <div class="row mt-5">
                            <div class="col-md-12">
                                <div class="timeline-main">
                                    <!-- Timeline Wrapper -->
                                    <ul class="stepper stepper-horizontal timeline timeline-basic pl-0">

                                        <li>
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle primary-color z-depth-1-half"><i class="fas fa-check" aria-hidden="true"></i></span>
                                            </a>

                                            <!-- Section Description -->
                                            <div class="step-content z-depth-1 ml-2 p-4">
                                                <h6 >Chinchilla</h6>
                                            </div>
                                        </li>
                                        <li class="timeline-inverted">
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle  z-depth-1-half mcircle"><i class="fas fa-bus-alt"></i></span>
                                            </a>
                                        </li>
                                        <li>
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle  z-depth-1-half mcircle"><i class="fas fa-bus-alt"></i></span>
                                            </a>
                                        </li>
                                        <li class="timeline-inverted">
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle  z-depth-1-half mcircle"><i class="fas fa-bus-alt"></i></span>
                                            </a>
                                        </li>
                                        <li>
                                            <!--Section Title -->
                                            <a href="#!">
                                                <span class="circle  z-depth-1-half mcircle"><i class="fas fa-check"></i></span>
                                            </a>

                                            <!-- Section Description -->
                                            <div class="step-content z-depth-1 ml-2 p-4 ">
                                                <h6>Albacete</h6>
                                            </div>
                                        </li>
                                    </ul>
                                    <!-- Timeline Wrapper -->
                                </div>
                            </div>
                        </div>
                        <!-- Timeline -->
                        <!-- Mask & flexbox options-->
                    </div>
                    <!-- Full Page Intro -->
                </div>
            </div>
        </header>
        <!-- Main navigation -->
        <main>
            <div class="container">

                <div class="row py-5">

                    <div class="col-md-12 text-center" id="contenedorViajes">
                        <h1>VIAJES</h1>
                        <div class="container">

                            <%Iterator Iterx = arrayViajes.iterator();

                                while (Iterx.hasNext()) {
                                    Viaje objViaje = (Viaje) Iterx.next();
                            %>
                            <%
                                String origen = objViaje.getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta().getLocalidadEstacion();
                                String destino = arrayViajes.get(0).getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta().getLocalidadEstacion();
                                int duracionRuta = objViaje.getHorario().getRuta().getDuracionRuta();
                                Date horaSalida = objViaje.getHorario().getHoraSalidaHorario();
                                double precioRuta = objViaje.getHorario().getRuta().getPrecioRuta();
                                Date fechaSalida = objViaje.getFechaViaje();

                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(objViaje.getHorario().getHoraSalidaHorario()); //tuFechaBase es un Date;
                                calendar.add(Calendar.MINUTE, objViaje.getHorario().getRuta().getDuracionRuta()); //minutosASumar es int.
                                Date fechaLLegada = calendar.getTime(); //Y ya tienes l
                                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                                String horaLLegada = dateFormat.format(fechaLLegada);

                                Calendar calendarHour = Calendar.getInstance(); // set this up however you need it.
                                calendarHour.setTime(objViaje.getHorario().getHoraSalidaHorario());
                                int hora = calendarHour.get(calendarHour.HOUR);

                                Calendar calendarMinutos = Calendar.getInstance(); // set this up however you need it.
                                calendarMinutos.setTime(objViaje.getHorario().getHoraSalidaHorario());
                                int minutos = calendarHour.get(calendarMinutos.MINUTE);

                                contador++;

                            %>
                            <div class="row py-5">
                                <div class="col-md-12 text-center viajes">
                                    <h4 class="textCont"><%=origen%> - <%=destino%></h4>
                                    <div class="container">
                                        <div class="row py-5">
                                            <div class="col-md-4 text-center" >

                                                <div class="clock">
                                                    <div class="hour"></div>
                                                    <div class="minute"></div>
                                                </div>

                                            </div>
                                            <div style="border: solid 1px;"></div>
                                            <div class="col-md-7 text-center">
                                                <!-- Excerpt -->
                                                <form action="../controladorViajerosAutobus">
                                                    <h4><strong>Horario:</strong> <i class="far fa-clock"></i>   <%=horaSalida%> --- <i class="far fa-clock"></i> <%=horaLLegada%></h4>
                                                    <h4><strong>Duración:</strong> <i class="fas fa-stopwatch"></i>   <%=duracionRuta%></h4>
                                                    <h4><strong>Precio:</strong>   <%=precioRuta%></h4>
                                                    <!-- Post data -->
                                                    <h4><a><strong> Fecha salida:</strong></a> <i class="far fa-calendar-check"></i>  <%=fechaSalida%></h4>
                                                    <!-- Read more button -->

                                                    <button value="<%=objViaje.getIdViaje()%>" class="btn aqua-gradient" name="botonReservaViaje">Reserva</button>
                                                </form>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <script>
                                var reloj = document.getElementsByClassName("clock");

                                var hour = document.getElementsByClassName("hour");
                                var minute = document.getElementsByClassName("minute");


                                var horas = "<%=hora%>";
                                var minutos = "<%=minutos%>";
                                var cont = "<%=contador%>" - 1;

                                var a = minutos * 6;
                                var o = horas % 12 / 12 * 360 + (a / 12);
                                hour[cont].style.transform = "rotate(" + o + "deg)";
                                minute[cont].style.transform = "rotate(" + a + "deg)";

                            </script>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- /Start your project here-->

        <!-- SCRIPTS -->
        <!-- JQuery -->
        <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="../js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="../js/mdb.min.js"></script>

        <script type='text/javascript' src='../js/compiled.0.min.js'></script>
        <script>

                                window.onload = function () {
                                    animation();


                                }
        </script>
        <script>

            function animation() {
                var circulos = document.getElementsByClassName("mcircle");

                var contador = 1;

                setInterval(x, 2000);

            }

            function x() {
                var circulos = document.getElementsByClassName("mcircle");
                circulos[0].classList.add("animated", "flash");

                circulos[0].classList.add("verde");
                circulos[0].classList.remove("mcircle");
                var i = document.createElement("i");
                
            }
        </script>

    </body>

</html>

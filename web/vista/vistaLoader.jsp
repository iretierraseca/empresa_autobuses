<%-- 
    Document   : vistaLoader
    Created on : 08-mar-2019, 19:39:39
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body{
                margin: 0;
                padding: 0;
                width: 100vw;
                height: 100vh;
            }
            
            ul{
                position:absolute;
                top:50%;
                left:50%;
                transform: translate(-50%,-50%);
                margin:0;
                padding:0;
                width: 600px;
                height: 150px;
            }
            
            ul li{
                list-style:none;
                position: absolute;
                width:100px;
                height:100px;
                background: #000;
                transform: rotate(45deg);
                transition:.5s;
                margin: 25px;
                overflow: hidden;
            }
            
            ul li.item00{
                top: -50%;
                left: 12.5%;
            }
            ul li.item01{
                top: -50%;
                left: 37.5%;
            }
            ul li.item02{
                top: -50%;
                left: 62.5%;
            }
            
            ul li.item1{
                top:0;
                left:0;
            }
            
            ul li.item2{
                bottom:0;
                left:25%;
            }
            ul li.item3{
                top:0;
                left:50%;
            }
            
            ul li.item4{
                bottom:0;
                left:75%;
            }
            
            ul li.item10{
                position: relative;
                bottom: -50%;
                left: 12.5%;
            }
            
            ul li.item11{
                bottom: -50%;
                left: 37.5%;
            }
            
            ul li.item12{
                bottom: -50%;
                left: 62.5%;
            }
            
            ul li .bg{
                width:100%;
                height: 100%;
                transform:rotate(-45deg) scale(1.42);
            }
            
            ul li.item00 .bg{
                background: url('../img/loader/item00.jpg');
                background-size:100px 100px;
            }
            
            ul li.item01 .bg{
                background: url('../img/loader/item01.jpg');
                background-size:100px 100px;
            }
            ul li.item02 .bg{
                background: url('../img/loader/item02.jpg');
                background-size:100px 100px;
            }
            
            ul li.item1 .bg{
                background: url('../img/loader/item1.jpg');
                background-size:100px 100px;
            }
            
            ul li.item2 .bg{
                background: url('../img/loader/item2.jpg');
                background-size:100px 100px;
            }
            
            ul li.item3 .bg{
                background: url('../img/loader/item3.jpg');
                background-size:100px 100px;
            }
            
            ul li.item4 .bg{
                background: url('../img/loader/item4.jpg');
                background-size:100px 100px;
            }
            
            ul li.item10 .bg{
                background: url('../img/loader/item10.jpg');
                background-size:100px 100px;
            }
            
            ul li.item11 .bg{
                background: url('../img/loader/item11.jpg');
                background-size:100px 100px;
            }
            
            ul li.item12 .bg{
                background: url('../img/loader/item13.jpg');
                background-size:100px 100px;
            }
            
        </style>
    </head>
    
    <body>
        <ul>
            <li class="item00"><div class="bg"></div></li>
            <li class="item01"><div class="bg"></div></li>
            <li class="item02"><div class="bg"></div></li>
            <li class="item1"><div class="bg"></div></li>
            <li class="item2"><div class="bg"></div></li>
            <li class="item3"><div class="bg"></div></li>
            <li class="item4"><div class="bg"></div></li>
            <li class="item10"><div class="bg"></div></li>
            <li class="item11"><div class="bg"></div></li>
            <li class="item12"><div class="bg"></div></li>
           
        </ul>

    </body>
</html>

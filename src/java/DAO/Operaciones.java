/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import MODELO.ExceptionGeneral;
import MODELO.ViajeServices;
import POJO.Administrador;
import POJO.Autobus;
import POJO.Backupviaje;
import POJO.Backupviajero;
import POJO.Cliente;
import POJO.Estacion;
import POJO.Ocupacion;
import POJO.Reserva;
import POJO.Ruta;
import POJO.Tarjeta;
import POJO.Viaje;
import POJO.Viajero;
import static com.sun.xml.bind.util.CalendarConv.formatter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.IntStream.range;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Irene
 */
public class Operaciones {

    /**
     * Obtiene un array con los origenes
     * 
     * @param _SessionBuilder
     * @return
     */
    public Set getOrigenRuta(SessionFactory _SessionBuilder) {
        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "from Ruta";
        Query q = sesion.createQuery(ordenHQL);
        List origenRuta = q.list();

        Iterator Iter = origenRuta.iterator();

        Set setEstaciones = new HashSet();
        while (Iter.hasNext()) {
            Ruta ruta = (Ruta) Iter.next();

            //Estacion estacion = (Estacion) sesion.load(Estacion.class, ruta.getEstacionByFkIdEstacionOrigenRuta().getIdEstacion());
            Hibernate.initialize(ruta.getEstacionByFkIdEstacionOrigenRuta());
            //Estacion estacion =  ruta.getEstacionByFkIdEstacionOrigenRuta();  

            setEstaciones.add(ruta.getEstacionByFkIdEstacionOrigenRuta());
            //ruta.setEstacionByFkIdEstacionOrigenRuta(estacion);
        }
        sesion.close();
        return setEstaciones;
    }

    /**
     * Obtiene los destinos segun el origen
     * 
     * @param _SessionBuilder
     * @param idOrigen
     * @return
     */
    public List<Estacion> dameDestinos(SessionFactory _SessionBuilder, int idOrigen) {

        Session sesion = _SessionBuilder.openSession();
        String hql = "select distinct r.estacionByFkIdEstacionDestinoRuta From Ruta r where r.estacionByFkIdEstacionOrigenRuta=" + idOrigen;
        Query q = sesion.createQuery(hql);
        List listRutas = q.list();
        sesion.close();
        return listRutas;
    }

    /**
     * Obtiene los destinos segun el origen
     * 
     * @param _SessionBuilder
     * @param _idOrigenRuta
     * @return
     */
    public List getDestinoRuta(SessionFactory _SessionBuilder, int _idOrigenRuta) {
        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "from Ruta r WHERE r.estacionByFkIdEstacionOrigenRuta=:vOrigen";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("vOrigen", _idOrigenRuta);
        List<Ruta> destinoRuta = q.list();

        for (Ruta item : destinoRuta) {
            Hibernate.initialize(item.getEstacionByFkIdEstacionDestinoRuta());
        }

        sesion.close();
        return destinoRuta;
    }

    /**
     * Obtiene los viajes 
     * 
     * @param _SessionBuilder
     * @param _idOrigenRuta
     * @param _idDestinoRuta
     * @param _fechaViaje
     * @param _numeroPasajeros
     * @return
     */
    public List getViajes(SessionFactory _SessionBuilder, int _idOrigenRuta, int _idDestinoRuta, Date _fechaViaje, int _numeroPasajeros) throws ExceptionGeneral {
        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Viaje AS viaje WHERE viaje.horario IN (SELECT horario.idHorario FROM Horario AS horario WHERE horario.ruta="
                + "(SELECT ruta.idRuta FROM Ruta AS ruta WHERE ruta.estacionByFkIdEstacionOrigenRuta=:origenRuta"
                + " AND ruta.estacionByFkIdEstacionDestinoRuta=:destinoRuta)) "
                + "AND viaje.fechaViaje=:fechaViaje AND viaje.plazasLibresViaje >=:numPasajeros";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("origenRuta", _idOrigenRuta);
        q.setParameter("destinoRuta", _idDestinoRuta);
        q.setParameter("fechaViaje", _fechaViaje);
        q.setParameter("numPasajeros", _numeroPasajeros);
        List<Viaje> viajes = q.list();

        for (Viaje item : viajes) {
            Hibernate.initialize(item.getHorario());
            Hibernate.initialize(item.getHorario().getRuta());
            Hibernate.initialize(item.getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta());
            Hibernate.initialize(item.getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta());
        }

        if (viajes.size() == 0) {
            throw new ExceptionGeneral(076, "No hay viajes disponibles");
        }

        sesion.close();
        return viajes;
    }

    /**
     *
     * @param _SessionBuilder
     * @param _idViaje
     * @return
     */
    public Viaje getViaje(SessionFactory _SessionBuilder, int _idViaje) {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Viaje AS viaje WHERE viaje.idViaje =:_idViaje";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("_idViaje", _idViaje);
        Viaje objViaje = (Viaje) q.uniqueResult();
        Hibernate.initialize(objViaje.getHorario());
        Hibernate.initialize(objViaje.getHorario().getRuta());
        Hibernate.initialize(objViaje.getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta());
        Hibernate.initialize(objViaje.getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta());
        sesion.close();
        return objViaje;

    }

    /**
     *
     * @param _SessionBuilder
     * @param _idViaje
     * @return
     */
    public List getAsientosOcupados(SessionFactory _SessionBuilder, int _idViaje) {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQLocupacion = "SELECT ocupacion.asientoOcupacion FROM Ocupacion AS ocupacion WHERE ocupacion.reserva IN "
                + "(SELECT DISTINCT reser.idReserva FROM Reserva AS reser WHERE reser.viaje=:idViaje)";
        Query qOcupacion = sesion.createQuery(ordenHQLocupacion);
        qOcupacion.setParameter("idViaje", _idViaje);
        List asientosOcupados = qOcupacion.list();

        return asientosOcupados;

    }

    /**
     *
     * @param _SessionBuilder
     * @return
     */
    public Autobus getNumeroPlazasAutobus(SessionFactory _SessionBuilder) {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "from Autobus";
        Query q = sesion.createQuery(ordenHQL);
        Autobus objAutobus = (Autobus) q.uniqueResult();
        sesion.close();

        return objAutobus;

    }

    /**
     * @deprecated @param _SessionBuilder
     * @param _emailCliente
     * @param _passCliente
     * @return
     * @throws MODELO.ExceptionGeneral
     */
    public Cliente getCliente(SessionFactory _SessionBuilder, String _emailCliente, String _passCliente) throws ExceptionGeneral {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Cliente AS cliente WHERE cliente.emailCliente =:emailCliente AND passCliente=:passCliente";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("emailCliente", _emailCliente);
        q.setParameter("passCliente", _passCliente);
        Cliente objCliente = (Cliente) q.uniqueResult();

        if (objCliente != null) {
            Hibernate.initialize(objCliente.getTarjetas());

            Set tarjeta = objCliente.getTarjetas();

            if (tarjeta.size() > 0) {
                Iterator Iterx = tarjeta.iterator();

                while (Iterx.hasNext()) {
                    Tarjeta tarj = (Tarjeta) Iterx.next();

                    tarj.setCliente(null);
                    tarj.setReservas(null);
                    tarj.setBackupreservas(null);
                }
            }

            objCliente.setTarjetas(tarjeta);

        }

        sesion.close();

        if (objCliente == null) {
            throw new ExceptionGeneral(075, "Debe registrarse antes");
        }

        return objCliente;

    }

    /**
     * 
     * @param _SessionBuilder
     * @param _emailCliente
     * @param _passCliente
     * @return
     * @throws ExceptionGeneral 
     */
    public Cliente getClienteLogin(SessionFactory _SessionBuilder, String _emailCliente, String _passCliente) throws ExceptionGeneral {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Cliente AS cliente WHERE cliente.emailCliente =:emailCliente AND passCliente=:passCliente";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("emailCliente", _emailCliente);
        q.setParameter("passCliente", _passCliente);
        Cliente objCliente = (Cliente) q.uniqueResult();

        if (objCliente != null) {
            Hibernate.initialize(objCliente.getTarjetas());
        }

        sesion.close();

        if (objCliente == null) {
            throw new ExceptionGeneral(075, "Debe registrarse antes");
        }

        return objCliente;

    }

    /**
     * @deprecated @param _SessionBuilder
     * @param _idTarjeta
     * @return
     */
    public Tarjeta getTarjetasCliente(SessionFactory _SessionBuilder, int _idTarjeta) throws ExceptionGeneral {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Tarjeta AS tarjeta WHERE tarjeta.idTarjeta =:idTarjeta";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("idTarjeta", _idTarjeta);
        Tarjeta objTarjeta = (Tarjeta) q.uniqueResult();

        Hibernate.initialize(objTarjeta.getCliente());

        Cliente objCliente = getCliente(_SessionBuilder, objTarjeta.getCliente().getEmailCliente(), objTarjeta.getCliente().getPassCliente());
        objTarjeta.setCliente(objCliente);

        objTarjeta.setReservas(null);
        objTarjeta.setBackupreservas(null);

        sesion.close();

        return objTarjeta;
    }

    /**
     *
     * @param _SessionBuilder
     * @param _objReserva
     */
    public void guardarReserva(SessionFactory _SessionBuilder, Reserva _objReserva) {
        Session sesion = _SessionBuilder.openSession();
        Transaction Tx = null;
        try {
            Tx = sesion.beginTransaction();
            sesion.saveOrUpdate(_objReserva);
            Tx.commit();
        } catch (HibernateException HE) {
            HE.printStackTrace();
            if (Tx != null) {
                Tx.rollback();
            }
            throw HE;
        } finally {
            sesion.close();
        }
    }

    /**
     * 
     * @param _identificador
     * @return 
     */
    public String comprobarNifNie(String _identificador) {

        String tipoIdentificadorCliente;
        boolean tipo = false;
        for (int i = 0; i < 10; i++) {

            if (_identificador.substring(0, 1).equals(String.valueOf(i))) {
                tipo = true;
            }
        }

        if (tipo == true) {
            tipoIdentificadorCliente = "NIF";
        } else {
            tipoIdentificadorCliente = "NIE";
        }
        return tipoIdentificadorCliente;
    }

    /**
     * 
     * @param _SessionBuilder
     * @param _identificador
     * @return 
     */
    public Viajero getViajero(SessionFactory _SessionBuilder, String _identificador) {
        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "From Viajero as viajero WHERE viajero.identificadorViajero=:identificador";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("identificador", _identificador);
        Viajero objViajero = (Viajero) q.uniqueResult();
        sesion.close();
        return objViajero;

    }

    /**
     * 
     * @param _SessionBuilder
     * @param _identificador
     * @param _emailCliente
     * @throws ExceptionGeneral 
     */
    public void getClienteComprobacion(SessionFactory _SessionBuilder, String _identificador, String _emailCliente) throws ExceptionGeneral {
        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "From Cliente as cliente WHERE cliente.identificadorCliente =:identificador";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("identificador", _identificador);
        Cliente objCliente = (Cliente) q.uniqueResult();

        String ordenHQLE = "From Cliente as cliente WHERE cliente.emailCliente =:email";
        Query qE = sesion.createQuery(ordenHQLE);
        qE.setParameter("email", _emailCliente);
        Cliente objClienteE = (Cliente) qE.uniqueResult();

        if (objCliente != null || objClienteE != null) {
            //Hibernate.initialize(objCliente.getTarjetas());
            throw new ExceptionGeneral(075, "El cliente ya esta Registrado. Debe hacer Login");

        }
        sesion.close();

    }

    /**
     * 
     * @param _SessionBuilder
     * @param _numeroTarjeta
     * @throws ExceptionGeneral 
     */
    public void getTarjeta(SessionFactory _SessionBuilder, String _numeroTarjeta) throws ExceptionGeneral {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "From Tarjeta as tarjeta WHERE tarjeta.numeroTarjeta =:numeroTarjeta";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("numeroTarjeta", _numeroTarjeta);
        Tarjeta objTarjeta = (Tarjeta) q.uniqueResult();
        sesion.close();

        if (objTarjeta != null) {
            throw new ExceptionGeneral(074, "La tarjeta introducida ya existe, si desea utilizara debe logearse");
        }
    }

    /**
     * 
     * @param _SessionBuilder
     * @param _numeroTarjeta
     * @return 
     */
    public Tarjeta getTarjetaComprobacion(SessionFactory _SessionBuilder, String _numeroTarjeta) {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "From Tarjeta as tarjeta WHERE tarjeta.numeroTarjeta =:numeroTarjeta";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("numeroTarjeta", _numeroTarjeta);
        Tarjeta objTarjeta = (Tarjeta) q.uniqueResult();
        sesion.close();

        return objTarjeta;
    }

    /**
     * 
     * @param _SessionBuilder
     * @return
     * @throws ExceptionGeneral 
     */
    public List<Viaje> getViajesDisponibles(SessionFactory _SessionBuilder) throws ExceptionGeneral {
        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Viaje AS viaje WHERE viaje.idViaje IN (SELECT reserva.viaje FROM Reserva AS reserva)";
        Query q = sesion.createQuery(ordenHQL);

        List<Viaje> viajes = q.list();

        for (Viaje item : viajes) {
            Hibernate.initialize(item.getHorario());
            Hibernate.initialize(item.getHorario().getRuta());
            Hibernate.initialize(item.getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta());
            Hibernate.initialize(item.getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta());
        }

        if (viajes.size() == 0) {
            throw new ExceptionGeneral(077, "No hay viajes disponibles");
        }

        return viajes;

    }

    /**
     * 
     * @param _estacionOrigen
     * @param _estacionDestino
     * @return 
     */
    public String getIdentificador(String _estacionOrigen, String _estacionDestino) {

        String letraEstacionOrigen = _estacionOrigen.substring(0, 2);
        String letraEstacionDestino = _estacionDestino.substring(0, 2);

        String cadenaIdentificador = letraEstacionOrigen;
        for (int i = 0; i < 6; i++) {
            int valorDado = (int) Math.floor(Math.random() * 6 + 1);
            cadenaIdentificador = cadenaIdentificador + valorDado;
        }
        cadenaIdentificador = cadenaIdentificador + letraEstacionDestino;
        return cadenaIdentificador;
    }

    /**
     * 
     * @param _SessionBuilder
     * @param _usuario
     * @param _pass
     * @throws ExceptionGeneral 
     */
    public void getAdministrador(SessionFactory _SessionBuilder, String _usuario, String _pass) throws ExceptionGeneral {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Administrador as adm WHERE adm.nombreAdministrador=:usuario AND adm.passAdministrador=:pass ";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("usuario", _usuario);
        q.setParameter("pass", _pass);
        Administrador objAdministrador = (Administrador) q.uniqueResult();

        sesion.close();

        if (objAdministrador == null) {
            throw new ExceptionGeneral(075, "Debe registrarse antes");
        }

    }

    /**
     * @deprecated @param _SessionBuilder
     * @param _idViaje
     */
    public void finalizarViaje(SessionFactory _SessionBuilder, int _idViaje) {
        Session sesion = _SessionBuilder.openSession();

        Transaction Tx = null;
        try {
            Tx = sesion.beginTransaction();

            String ordenHQL = "delete FROM Viaje WHERE idViaje=:idViaje";

            Query q = sesion.createSQLQuery(ordenHQL);
            q.setParameter("idViaje", _idViaje);
            int rowCount = q.executeUpdate();
            Tx.commit();
        } catch (HibernateException HE) {
            HE.printStackTrace();
            if (Tx != null) {
                Tx.rollback();
            }
            throw HE;
        } finally {
            sesion.close();
        }
    }

    /**
     * 
     * @param _SessionBuilder
     * @param _idViaje
     * @return 
     */
    public Viaje getViajeFinalizar(SessionFactory _SessionBuilder, int _idViaje) {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Viaje AS viaje WHERE viaje.idViaje =:_idViaje";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("_idViaje", _idViaje);
        Viaje objViaje = (Viaje) q.uniqueResult();
        Hibernate.initialize(objViaje.getHorario());
        Hibernate.initialize(objViaje.getHorario().getRuta());
        Hibernate.initialize(objViaje.getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta());
        Hibernate.initialize(objViaje.getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta());
        Hibernate.initialize(objViaje.getReservas());
        Set<Reserva> objReservas = (Set<Reserva>) objViaje.getReservas();

        for (Reserva objReserva : objReservas) {
            Hibernate.initialize(objReserva.getTarjeta());
            Hibernate.initialize(objReserva.getOcupacions());
            Set<Ocupacion> objOcupaciones = (Set<Ocupacion>) objReserva.getOcupacions();

            for (Ocupacion objOcupacion : objOcupaciones) {
                Hibernate.initialize(objOcupacion.getViajero());
            }

        }

        sesion.close();
        return objViaje;

    }

    /**
     * 
     * @param _SessionBuilder
     * @param _identificador
     * @return 
     */
    public Backupviajero getBackupViajeroComprobacion(SessionFactory _SessionBuilder, String _identificador) {

        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "From Backupviajero WHERE identificadorBackupViajero=:identificador";
        Query q = sesion.createQuery(ordenHQL);
        q.setParameter("identificador", _identificador);
        Backupviajero objBackupViajero = (Backupviajero) q.uniqueResult();

        if (objBackupViajero != null) {
            Hibernate.initialize(objBackupViajero.getBackupocupacions());
        }
        sesion.close();
        return objBackupViajero;

    }

    /**
     * 
     * @param _SessionBuilder
     * @param _objBackupViaje
     * @param _objViaje 
     */
    public void finalizarViaje(SessionFactory _SessionBuilder, Backupviaje _objBackupViaje, Viaje _objViaje) {
        Session sesion = _SessionBuilder.openSession();
        Transaction Tx = null;
        try {
            Tx = sesion.beginTransaction();
            sesion.saveOrUpdate(_objBackupViaje);
            sesion.delete(_objViaje);

            String ordenHQL = "FROM Viajero AS viajero WHERE viajero.idViajero NOT IN (SELECT ocupacion.viajero FROM Ocupacion AS ocupacion)";
            Query q = sesion.createQuery(ordenHQL);
            List<Viajero> objsViajero = q.list();

            for (Viajero objViajero : objsViajero) {
                sesion.delete(objViajero);
            }

            Tx.commit();
        } catch (HibernateException HE) {
            HE.printStackTrace();
            if (Tx != null) {
                Tx.rollback();
            }
            throw HE;
        } finally {
            sesion.close();
        }
    }

    /**
     * 
     * @return 
     */
    public List<ViajeServices> servicioWeb() {

        SessionFactory _SessionBuilder = NewHibernateUtil.getSessionFactory();
        Session sesion = _SessionBuilder.openSession();

        String ordenHQL = "FROM Viaje AS viaje";
        Query q = sesion.createQuery(ordenHQL);
        List<Viaje> objsViajes = q.list();
        List<ViajeServices> viajesServices = new ArrayList();
        for (Viaje objViaje : objsViajes) {
            Hibernate.initialize(objViaje.getHorario());
            Hibernate.initialize(objViaje.getHorario().getRuta());
            Hibernate.initialize(objViaje.getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta());
            Hibernate.initialize(objViaje.getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta());
            Hibernate.initialize(objViaje.getReservas());
            Set<Reserva> objReservas = (Set<Reserva>) objViaje.getReservas();

            for (Reserva objReserva : objReservas) {
                Hibernate.initialize(objReserva.getTarjeta());
                Hibernate.initialize(objReserva.getOcupacions());
                Set<Ocupacion> objOcupaciones = (Set<Ocupacion>) objReserva.getOcupacions();

                for (Ocupacion objOcupacion : objOcupaciones) {
                    Hibernate.initialize(objOcupacion.getViajero());
                }

            }
            
            String origen = objViaje.getHorario().getRuta().getEstacionByFkIdEstacionOrigenRuta().getLocalidadEstacion();
            String destino = objViaje.getHorario().getRuta().getEstacionByFkIdEstacionDestinoRuta().getLocalidadEstacion();
            Date dia = objViaje.getFechaViaje();
            
            
            
            Date horaSalida = objViaje.getHorario().getHoraSalidaHorario();
            int numeroPlazasLibres = objViaje.getPlazasLibresViaje();
            ViajeServices objViajeServices = new ViajeServices(origen,destino,dia,horaSalida,numeroPlazasLibres);
            viajesServices.add(objViajeServices);
            
        }

        return viajesServices;

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAO.NewHibernateUtil;
import DAO.Operaciones;
import POJO.Autobus;
import POJO.Estacion;
import POJO.Ruta;
import POJO.Viaje;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Irene
 */
public class controladorViajerosAutobus extends HttpServlet {
    
     private SessionFactory SessionBuilder;

    public void init() {
        SessionBuilder = NewHibernateUtil.getSessionFactory();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            //Session sesion = SessionBuilder.openSession();
            /* TODO output your page here. You may use following sample code. */
            int idViaje = Integer.parseInt(request.getParameter("botonReservaViaje"));
            
            //out.print(idViaje);
            
            List arrayAsientosOcupados = new Operaciones().getAsientosOcupados(SessionBuilder, idViaje);
            Autobus objAutobus = new Operaciones().getNumeroPlazasAutobus(SessionBuilder);
            Viaje objViaje = new Operaciones().getViaje(SessionBuilder, idViaje);
            
            HttpSession arraySession = request.getSession(true);
            arraySession.setAttribute("asientosOcupados", arrayAsientosOcupados);
            arraySession.setAttribute("plazasAutobus", objAutobus.getNumeroPlazasAutobus());
            arraySession.setAttribute("viajeElegido", objViaje);
            
            response.sendRedirect("vista/vistaViajeros.jsp");
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

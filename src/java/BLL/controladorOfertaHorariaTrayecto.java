/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAO.NewHibernateUtil;
import DAO.Operaciones;
import MODELO.ExceptionGeneral;
import POJO.Horario;
import POJO.Viaje;
import java.io.IOException;
import java.io.PrintWriter;
/*import java.text.SimpleDateFormat;*/
/*import java.util.Calendar;*/
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Irene
 */
public class controladorOfertaHorariaTrayecto extends HttpServlet {

    private SessionFactory SessionBuilder;

    public void init() {
        SessionBuilder = NewHibernateUtil.getSessionFactory();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            int idEstacionOrigen = Integer.parseInt(request.getParameter("selectOrigen"));
            int idEstacionDestino = Integer.parseInt(request.getParameter("selectDestino"));
            Date fechaViaje = java.sql.Date.valueOf(request.getParameter("fechaViaje"));
            int numeroPasajeros = Integer.parseInt(request.getParameter("numeroPasajeros"));

            try{
                List viajes = new Operaciones().getViajes(SessionBuilder, idEstacionOrigen, idEstacionDestino, fechaViaje, numeroPasajeros);

                HttpSession arraySession = request.getSession(true);
                arraySession.setAttribute("viajes", viajes);
                arraySession.setAttribute("numeroPasajeros",numeroPasajeros);

                response.sendRedirect("vista/vistaViaje.jsp");
            }catch(ExceptionGeneral ex){
                response.sendRedirect("index.jsp");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

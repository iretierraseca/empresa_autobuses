/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAO.NewHibernateUtil;
import DAO.Operaciones;
import MODELO.AES;
import MODELO.ExceptionGeneral;
import MODELO.ViajeroAsiento;
import POJO.Cliente;
import POJO.Ocupacion;
import POJO.Reserva;
import POJO.Tarjeta;
import POJO.Viaje;
import POJO.Viajero;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.SessionFactory;

/**
 *
 * @author Irene
 */
public class controladorPagarLogin extends HttpServlet {
    
     private SessionFactory SessionBuilder;

    public void init() {
        SessionBuilder = NewHibernateUtil.getSessionFactory();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            String nombrePTarjeta = request.getParameter("name");
            String numeroTarjeta = request.getParameter("cardnumber");
            
            String secretKey = "tierraseca";
     
            String encryptedString = AES.encrypt(numeroTarjeta, secretKey) ;
            numeroTarjeta = encryptedString;
            
            String fechaExpiracionTarjeta = request.getParameter("expirationdate");
            String cvvTarjeta = request.getParameter("securitycode");
            String tipoTarjeta = request.getParameter("radioTipoTarjeta");
            
            HttpSession arraySession = request.getSession(true);
            //No comprobar si existe, controlado al hacer login
            Cliente objCliente = (Cliente) arraySession.getAttribute("cliente");
            
           
            Tarjeta objTarjeta = new Operaciones().getTarjetaComprobacion(SessionBuilder, numeroTarjeta);
            
            if(objTarjeta == null){
                objTarjeta = new Tarjeta(objCliente,tipoTarjeta,numeroTarjeta,fechaExpiracionTarjeta,null);
                objCliente.setTarjeta(objTarjeta);
            }
            
            //objeto Viaje
            Viaje objViaje = (Viaje) arraySession.getAttribute("viajeElegido");
            //Lista de los viajeros
            ArrayList<ViajeroAsiento> viajeros = (ArrayList<ViajeroAsiento>) arraySession.getAttribute("viajerosAsiento");
            //Objeto Reserva
                //Numero viajeros
                int numeroViajerosReserva = viajeros.size();
                //Actualizar plazas libres viaje
                objViaje.setPlazasLibresViaje(objViaje.getPlazasLibresViaje()-numeroViajerosReserva);
                //Fecha actual
                Date fecha = new Date();
                java.sql.Date fechaSQL = new java.sql.Date(fecha.getTime());
                //Precio reserva
                int total = (int) ((objViaje.getHorario().getRuta().getPrecioRuta())*numeroViajerosReserva);
                //Importe individual
                int importeIndividual = total/numeroViajerosReserva;
                //Localizador reserva
                String localizadorReserva = "123456789";
            
            Reserva objReserva = new Reserva(objTarjeta,objViaje,localizadorReserva,total,fechaSQL,numeroViajerosReserva);
            Ocupacion objOcupacion;
            for(ViajeroAsiento item: viajeros){
                Viajero objViajero = new Operaciones().getViajero(SessionBuilder, item.getIdentificadorViajero());
                
                if(objViajero == null){
                    Viajero objNuevoViajero = new Viajero(item.getTipoIdentificadorViajero(),item.getIdentificadorViajero(),item.getNombreViajero(),item.getApellidosViajero());
                    int asientoViajero = item.getAsiento();
                    objOcupacion = new Ocupacion(objReserva,objNuevoViajero,asientoViajero,importeIndividual);
                }else{
                    int asientoViajero = item.getAsiento();
                    objOcupacion = new Ocupacion(objReserva,objViajero,asientoViajero,importeIndividual);
                }
                objReserva.setOcupacion(objOcupacion);
            }
            
            new Operaciones().guardarReserva(SessionBuilder, objReserva);
            arraySession.setAttribute("objReserva", objReserva);
            
            response.sendRedirect("vista/vistaBillete.jsp");
           
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

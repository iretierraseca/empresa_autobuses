/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAO.NewHibernateUtil;
import DAO.Operaciones;
import MODELO.ViajeroAsiento;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.SessionFactory;

/**
 *
 * @author Irene
 */
public class controladorResumenCompra extends HttpServlet {
    
    private SessionFactory SessionBuilder;

    public void init() {
        SessionBuilder = NewHibernateUtil.getSessionFactory();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession arraySession = request.getSession(true);
            int numeroPasajeros = (Integer) arraySession.getAttribute("numeroPasajeros");
            
            ArrayList<ViajeroAsiento> arrayViajeroAsiento = new ArrayList();
            
            
            for(int i=0, j=1; i<numeroPasajeros;i++,j++){
                
                String numIdentificador = request.getParameter("numIdentificadorViajero"+j);
                String tipoIdentificador = request.getParameter("tipoIdentificador"+j);
                String nombreViajero = request.getParameter("nombreViajero"+j);
                String apellidosViajero = request.getParameter("apellidosViajero"+j);
                int asientoViajero = Integer.parseInt(request.getParameter("asientosViajero"+j));
                
                ViajeroAsiento objViajeroAsiento = new ViajeroAsiento(tipoIdentificador, numIdentificador, nombreViajero, apellidosViajero, asientoViajero);
                arrayViajeroAsiento.add(objViajeroAsiento);
            }
            
            arraySession.setAttribute("viajerosAsiento", arrayViajeroAsiento);
            
            response.sendRedirect("vista/vistaResumenCompra.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

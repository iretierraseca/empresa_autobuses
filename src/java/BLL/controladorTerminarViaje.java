/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAO.NewHibernateUtil;
import DAO.Operaciones;
import POJO.Backupocupacion;
import POJO.Backupreserva;
import POJO.Backupviaje;
import POJO.Backupviajero;
import POJO.Ocupacion;
import POJO.Reserva;
import POJO.Viaje;
import POJO.Viajero;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.SessionFactory;

/**
 *
 * @author Irene
 */
public class controladorTerminarViaje extends HttpServlet {
    
    private SessionFactory SessionBuilder;

    public void init() {
        SessionBuilder = NewHibernateUtil.getSessionFactory();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            int idViaje = Integer.parseInt(request.getParameter("selectViajesD"));
            
            Viaje objViaje = new Operaciones().getViajeFinalizar(SessionBuilder, idViaje);
            
            Backupviaje objBackupViaje = new Backupviaje(objViaje.getHorario(),objViaje.getFechaViaje(),objViaje.getPlazasLibresViaje());
            // OJO Añadir reservas a viajes
            Set <Reserva> objReservas = objViaje.getReservas();
            
                for(Reserva objReserva:objReservas){
                    Backupreserva objBackupReserva = new Backupreserva(objBackupViaje,objReserva.getTarjeta(),objReserva.getLocalizadorReserva(),objReserva.getPrecioReserva(),objReserva.getFechaPagoReserva(),objReserva.getNumViajerosReserva());
                    objBackupViaje.setBackupreservas(objBackupReserva);
                    
                    Set <Ocupacion> objOcupaciones = objReserva.getOcupacions();
                    
                    for(Ocupacion objOcupacion:objOcupaciones){
                        Viajero objViajero = objOcupacion.getViajero();
                        //Backupviajero objBackupViajero = new Backupviajero(objViajero.getTipoIdentificadorViajero(),objViajero.getIdentificadorViajero(),objViajero.getNombreViajero(),objViajero.getApellidosViajero());
                        
                        //comprobar si ya existe el viajero en backup
                        
                        Backupviajero objBackupViajero = new Operaciones().getBackupViajeroComprobacion(SessionBuilder, objViajero.getIdentificadorViajero());
                        
                        if(objBackupViajero == null){
                            Backupviajero objBackupNuevoViajero = new Backupviajero(objViajero.getTipoIdentificadorViajero(),objViajero.getIdentificadorViajero(),objViajero.getNombreViajero(),objViajero.getApellidosViajero());
                        
                            Backupocupacion objBackupOcupacion = new Backupocupacion(objBackupReserva,objBackupNuevoViajero,objOcupacion.getAsientoOcupacion(),objOcupacion.getImporteIndividualOcupacion());
                            objBackupNuevoViajero.setBackupocupacions(objBackupOcupacion);
                            objBackupReserva.setBackupocupacions(objBackupOcupacion);
                        }else{
               
                            Backupocupacion objBackupOcupacion = new Backupocupacion(objBackupReserva,objBackupViajero,objOcupacion.getAsientoOcupacion(),objOcupacion.getImporteIndividualOcupacion());
                            objBackupViajero.setBackupocupacions(objBackupOcupacion);
                            objBackupReserva.setBackupocupacions(objBackupOcupacion);
                        }
                       
                    }
                    
                    objBackupViaje.setBackupreservas(objBackupReserva);
                }
            
                new Operaciones().finalizarViaje(SessionBuilder, objBackupViaje, objViaje);
            
            
            response.sendRedirect("index.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

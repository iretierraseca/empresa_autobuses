/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVICES;

import DAO.NewHibernateUtil;
import DAO.Operaciones;
import MODELO.ViajeServices;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hibernate.SessionFactory;

/**
 *
 * @author Irene
 */
@WebService(serviceName = "WebServiceBus")
public class WebServiceBus {
    
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "informacionViajes")
    public List<ViajeServices> informacionViajes() {
        List<ViajeServices> x = new Operaciones().servicioWeb();
        
        return x;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

import java.util.Date;

/**
 *
 * @author Irene
 */
public class ViajeServices {
    private String origen;
    private String destino;
    private Date dias;
    private Date horaSalida;
    private int numeroPlazasLibres;

    public ViajeServices() {
    }

    public ViajeServices(String origen, String destino, Date dias, Date horaSalida, int numeroPlazasLibres) {
        this.origen = origen;
        this.destino = destino;
        this.dias = dias;
        this.horaSalida = horaSalida;
        this.numeroPlazasLibres = numeroPlazasLibres;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Date getDias() {
        return dias;
    }

    public void setDias(Date dias) {
        this.dias = dias;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public int getNumeroPlazasLibres() {
        return numeroPlazasLibres;
    }

    public void setNumeroPlazasLibres(int numeroPlazasLibres) {
        this.numeroPlazasLibres = numeroPlazasLibres;
    }

    

    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Irene
 */
import POJO.Viajero;
public class ViajeroAsiento extends Viajero{
    int asiento;

    public ViajeroAsiento(String tipoIdentificadorViajero, String identificadorViajero, String nombreViajero, String apellidosViajero, int asiento) {
        super(tipoIdentificadorViajero, identificadorViajero, nombreViajero, apellidosViajero);
        this.asiento = asiento;
    }

    public int getAsiento() {
        return asiento;
    }

    public void setAsiento(int asiento) {
        this.asiento = asiento;
    }
    
    
    
}

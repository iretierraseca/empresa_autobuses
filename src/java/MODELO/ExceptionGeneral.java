/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Irene
 */
public class ExceptionGeneral extends Exception{
    private int codigoError;
    private String mensajeError;

    public ExceptionGeneral(int codigoError, String mensajeError) {
        this.codigoError = codigoError;
        this.mensajeError = mensajeError;
    }

    @Override
    public String toString() {
        return "AppErrorException:{" +" " + mensajeError + '}';
    }
    
    
}

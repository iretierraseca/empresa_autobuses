package POJO;
// Generated 25-feb-2019 20:43:39 by Hibernate Tools 4.3.1



/**
 * Administrador generated by hbm2java
 */
public class Administrador  implements java.io.Serializable {


     private Integer idAdministrador;
     private String nombreAdministrador;
     private String passAdministrador;

    public Administrador() {
    }

    /**
     * 
     * @param nombreAdministrador Define el nombre del administrador
     * @param passAdministrador Define la contraseña del administrador
     */
    public Administrador(String nombreAdministrador, String passAdministrador) {
       this.nombreAdministrador = nombreAdministrador;
       this.passAdministrador = passAdministrador;
    }
   
    public Integer getIdAdministrador() {
        return this.idAdministrador;
    }
    
    public void setIdAdministrador(Integer idAdministrador) {
        this.idAdministrador = idAdministrador;
    }
    public String getNombreAdministrador() {
        return this.nombreAdministrador;
    }
    
    public void setNombreAdministrador(String nombreAdministrador) {
        this.nombreAdministrador = nombreAdministrador;
    }
    public String getPassAdministrador() {
        return this.passAdministrador;
    }
    
    public void setPassAdministrador(String passAdministrador) {
        this.passAdministrador = passAdministrador;
    }




}



package POJO;
// Generated 25-feb-2019 20:43:39 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Horario generated by hbm2java
 */
public class Horario  implements java.io.Serializable {


     private Integer idHorario;
     private Ruta ruta;
     private Date horaSalidaHorario;
     private String diaSemanaHorario;
     private String comentario;
     private Set backupviajes = new HashSet(0);
     private Set viajes = new HashSet(0);

    public Horario() {
    }

	
    /**
     * 
     * @param ruta
     * @param horaSalidaHorario
     * @param diaSemanaHorario 
     */
    public Horario(Ruta ruta, Date horaSalidaHorario, String diaSemanaHorario) {
        this.ruta = ruta;
        this.horaSalidaHorario = horaSalidaHorario;
        this.diaSemanaHorario = diaSemanaHorario;
    }
    /**
     * 
     * @param ruta
     * @param horaSalidaHorario
     * @param diaSemanaHorario
     * @param comentario
     * @param backupviajes
     * @param viajes 
     */
    public Horario(Ruta ruta, Date horaSalidaHorario, String diaSemanaHorario, String comentario, Set backupviajes, Set viajes) {
       this.ruta = ruta;
       this.horaSalidaHorario = horaSalidaHorario;
       this.diaSemanaHorario = diaSemanaHorario;
       this.comentario = comentario;
       this.backupviajes = backupviajes;
       this.viajes = viajes;
    }
   
    public Integer getIdHorario() {
        return this.idHorario;
    }
    
    public void setIdHorario(Integer idHorario) {
        this.idHorario = idHorario;
    }
    public Ruta getRuta() {
        return this.ruta;
    }
    
    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }
    public Date getHoraSalidaHorario() {
        return this.horaSalidaHorario;
    }
    
    public void setHoraSalidaHorario(Date horaSalidaHorario) {
        this.horaSalidaHorario = horaSalidaHorario;
    }
    public String getDiaSemanaHorario() {
        return this.diaSemanaHorario;
    }
    
    public void setDiaSemanaHorario(String diaSemanaHorario) {
        this.diaSemanaHorario = diaSemanaHorario;
    }
    public String getComentario() {
        return this.comentario;
    }
    
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    public Set getBackupviajes() {
        return this.backupviajes;
    }
    
    public void setBackupviajes(Set backupviajes) {
        this.backupviajes = backupviajes;
    }
    public Set getViajes() {
        return this.viajes;
    }
    
    public void setViajes(Set viajes) {
        this.viajes = viajes;
    }




}



package POJO;
// Generated 25-feb-2019 20:43:39 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Tarjeta generated by hbm2java
 */
public class Tarjeta  implements java.io.Serializable {


     private Integer idTarjeta;
     private Cliente cliente;
     private String tipoTarjeta;
     private String numeroTarjeta;
     private String caducidadTarjeta;
     private String logoTarjeta;
     private Set backupreservas = new HashSet(0);
     private Set reservas = new HashSet(0);

    public Tarjeta() {
    }

    /**
     * 
     * @param cliente
     * @param tipoTarjeta
     * @param numeroTarjeta
     * @param caducidadTarjeta
     * @param logoTarjeta 
     */
    public Tarjeta(Cliente cliente, String tipoTarjeta, String numeroTarjeta, String caducidadTarjeta, String logoTarjeta) {
        this.cliente = cliente;
        this.tipoTarjeta = tipoTarjeta;
        this.numeroTarjeta = numeroTarjeta;
        this.caducidadTarjeta = caducidadTarjeta;
        this.logoTarjeta = logoTarjeta;
    }
    
    /**
     * 
     * @param tipoTarjeta
     * @param numeroTarjeta
     * @param caducidadTarjeta
     * @param logoTarjeta 
     */
     public Tarjeta( String tipoTarjeta, String numeroTarjeta, String caducidadTarjeta, String logoTarjeta) {
        
        this.tipoTarjeta = tipoTarjeta;
        this.numeroTarjeta = numeroTarjeta;
        this.caducidadTarjeta = caducidadTarjeta;
        this.logoTarjeta = logoTarjeta;
    }
    
    

	/**
         * 
         * @param cliente
         * @param tipoTarjeta
         * @param numeroTarjeta
         * @param caducidadTarjeta 
         */
    public Tarjeta(Cliente cliente, String tipoTarjeta, String numeroTarjeta, String caducidadTarjeta) {
        this.cliente = cliente;
        this.tipoTarjeta = tipoTarjeta;
        this.numeroTarjeta = numeroTarjeta;
        this.caducidadTarjeta = caducidadTarjeta;
    }
    /**
     * 
     * @param cliente
     * @param tipoTarjeta
     * @param numeroTarjeta
     * @param caducidadTarjeta
     * @param logoTarjeta
     * @param backupreservas
     * @param reservas 
     */
    public Tarjeta(Cliente cliente, String tipoTarjeta, String numeroTarjeta, String caducidadTarjeta, String logoTarjeta, Set backupreservas, Set reservas) {
       this.cliente = cliente;
       this.tipoTarjeta = tipoTarjeta;
       this.numeroTarjeta = numeroTarjeta;
       this.caducidadTarjeta = caducidadTarjeta;
       this.logoTarjeta = logoTarjeta;
       this.backupreservas = backupreservas;
       this.reservas = reservas;
    }


    


   
    public Integer getIdTarjeta() {
        return this.idTarjeta;
    }
    
    public void setIdTarjeta(Integer idTarjeta) {
        this.idTarjeta = idTarjeta;
    }
    public Cliente getCliente() {
        return this.cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public String getTipoTarjeta() {
        return this.tipoTarjeta;
    }
    
    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }
    public String getNumeroTarjeta() {
        return this.numeroTarjeta;
    }
    
    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }
    public String getCaducidadTarjeta() {
        return this.caducidadTarjeta;
    }
    
    public void setCaducidadTarjeta(String caducidadTarjeta) {
        this.caducidadTarjeta = caducidadTarjeta;
    }
    public String getLogoTarjeta() {
        return this.logoTarjeta;
    }
    
    public void setLogoTarjeta(String logoTarjeta) {
        this.logoTarjeta = logoTarjeta;
    }
    public Set getBackupreservas() {
        return this.backupreservas;
    }
    
    public void setBackupreservas(Set backupreservas) {
        this.backupreservas = backupreservas;
    }
    public Set getReservas() {
        return this.reservas;
    }
    
    public void setReservas(Set reservas) {
        this.reservas = reservas;
    }




}



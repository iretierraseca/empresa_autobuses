-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.23-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para bd_autobus_igt_2018
DROP DATABASE IF EXISTS `bd_autobus_igt_2018`;
CREATE DATABASE IF NOT EXISTS `bd_autobus_igt_2018` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bd_autobus_igt_2018`;

-- Volcando estructura para tabla bd_autobus_igt_2018.administrador
DROP TABLE IF EXISTS `administrador`;
CREATE TABLE IF NOT EXISTS `administrador` (
  `idAdministrador` int(11) NOT NULL AUTO_INCREMENT,
  `nombreAdministrador` varchar(50) NOT NULL,
  `passAdministrador` varchar(50) NOT NULL,
  PRIMARY KEY (`idAdministrador`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.administrador: ~0 rows (aproximadamente)
DELETE FROM `administrador`;
/*!40000 ALTER TABLE `administrador` DISABLE KEYS */;
INSERT INTO `administrador` (`idAdministrador`, `nombreAdministrador`, `passAdministrador`) VALUES
	(1, 'admin', 'admin');
/*!40000 ALTER TABLE `administrador` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.autobus
DROP TABLE IF EXISTS `autobus`;
CREATE TABLE IF NOT EXISTS `autobus` (
  `idAutobus` int(11) NOT NULL AUTO_INCREMENT,
  `numeroPlazasAutobus` int(11) NOT NULL,
  PRIMARY KEY (`idAutobus`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.autobus: ~0 rows (aproximadamente)
DELETE FROM `autobus`;
/*!40000 ALTER TABLE `autobus` DISABLE KEYS */;
INSERT INTO `autobus` (`idAutobus`, `numeroPlazasAutobus`) VALUES
	(1, 8);
/*!40000 ALTER TABLE `autobus` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.backupocupacion
DROP TABLE IF EXISTS `backupocupacion`;
CREATE TABLE IF NOT EXISTS `backupocupacion` (
  `idBackupOcupacion` int(11) NOT NULL AUTO_INCREMENT,
  `fkIdReservaBackupOcupacion` int(11) NOT NULL,
  `fkIdViajeroBackupOcupacion` int(11) NOT NULL,
  `asientoBackupOcupacion` int(11) NOT NULL,
  `importeIndividualBackupOcupacion` int(11) NOT NULL,
  PRIMARY KEY (`idBackupOcupacion`),
  UNIQUE KEY `reservaviajeroasiento` (`fkIdReservaBackupOcupacion`,`fkIdViajeroBackupOcupacion`,`asientoBackupOcupacion`),
  KEY `FK_backupocupacion_reserva` (`fkIdReservaBackupOcupacion`),
  KEY `FK_backupocupacion_viajero` (`fkIdViajeroBackupOcupacion`),
  CONSTRAINT `FK_backupocupacion_reserva` FOREIGN KEY (`fkIdReservaBackupOcupacion`) REFERENCES `backupreserva` (`idBackupReserva`),
  CONSTRAINT `FK_backupocupacion_viajero` FOREIGN KEY (`fkIdViajeroBackupOcupacion`) REFERENCES `backupviajero` (`idBackupViajero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Volcando datos para la tabla bd_autobus_igt_2018.backupocupacion: ~0 rows (aproximadamente)
DELETE FROM `backupocupacion`;
/*!40000 ALTER TABLE `backupocupacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `backupocupacion` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.backupreserva
DROP TABLE IF EXISTS `backupreserva`;
CREATE TABLE IF NOT EXISTS `backupreserva` (
  `idBackupReserva` int(11) NOT NULL AUTO_INCREMENT,
  `fkIdViajeBackupReserva` int(11) NOT NULL,
  `fkIdTarjetaBackupReserva` int(11) NOT NULL,
  `localizadorBackupReserva` varchar(50) NOT NULL,
  `precioBackupReserva` int(11) NOT NULL,
  `fechaPagoBackupReserva` date NOT NULL,
  `numViajerosBackupReserva` int(11) NOT NULL,
  PRIMARY KEY (`idBackupReserva`),
  KEY `FK_backupreserva_tarjeta` (`fkIdTarjetaBackupReserva`),
  KEY `FK_backupreserva_backupviaje` (`fkIdViajeBackupReserva`),
  CONSTRAINT `FK_backupreserva_backupviaje` FOREIGN KEY (`fkIdViajeBackupReserva`) REFERENCES `backupviaje` (`idBackupViaje`),
  CONSTRAINT `FK_backupreserva_tarjeta` FOREIGN KEY (`fkIdTarjetaBackupReserva`) REFERENCES `tarjeta` (`idTarjeta`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Volcando datos para la tabla bd_autobus_igt_2018.backupreserva: ~0 rows (aproximadamente)
DELETE FROM `backupreserva`;
/*!40000 ALTER TABLE `backupreserva` DISABLE KEYS */;
/*!40000 ALTER TABLE `backupreserva` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.backupviaje
DROP TABLE IF EXISTS `backupviaje`;
CREATE TABLE IF NOT EXISTS `backupviaje` (
  `idBackupViaje` int(11) NOT NULL AUTO_INCREMENT,
  `fkIdHorarioBackupViaje` int(11) NOT NULL,
  `fechaBackupViaje` date NOT NULL,
  `plazasLibresBackupViaje` int(11) NOT NULL,
  PRIMARY KEY (`idBackupViaje`),
  KEY `FK_backupviaje_horario` (`fkIdHorarioBackupViaje`),
  CONSTRAINT `FK_backupviaje_horario` FOREIGN KEY (`fkIdHorarioBackupViaje`) REFERENCES `horario` (`idHorario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Volcando datos para la tabla bd_autobus_igt_2018.backupviaje: ~0 rows (aproximadamente)
DELETE FROM `backupviaje`;
/*!40000 ALTER TABLE `backupviaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `backupviaje` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.backupviajero
DROP TABLE IF EXISTS `backupviajero`;
CREATE TABLE IF NOT EXISTS `backupviajero` (
  `idBackupViajero` int(11) NOT NULL AUTO_INCREMENT,
  `tipoIdentificadorBackupViajero` enum('NIF','NIE','PASAPORTE') NOT NULL,
  `identificadorBackupViajero` varchar(50) NOT NULL,
  `nombreBackupViajero` varchar(50) NOT NULL,
  `apellidosBackupViajero` varchar(50) NOT NULL,
  PRIMARY KEY (`idBackupViajero`),
  UNIQUE KEY `identificadorBackupViajero` (`identificadorBackupViajero`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Volcando datos para la tabla bd_autobus_igt_2018.backupviajero: ~0 rows (aproximadamente)
DELETE FROM `backupviajero`;
/*!40000 ALTER TABLE `backupviajero` DISABLE KEYS */;
/*!40000 ALTER TABLE `backupviajero` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.cliente
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `tipoIdentificadorCliente` enum('NIF','NIE','PASAPORTE') NOT NULL,
  `identificadorCliente` varchar(9) NOT NULL,
  `nombreCliente` varchar(30) NOT NULL,
  `apellidosCliente` varchar(50) NOT NULL,
  `emailCliente` varchar(50) NOT NULL,
  `passCliente` varchar(150) NOT NULL,
  `telefonoCliente` varchar(9) NOT NULL,
  PRIMARY KEY (`idCliente`),
  UNIQUE KEY `identificadorCliente` (`identificadorCliente`),
  UNIQUE KEY `emailCliente` (`emailCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.cliente: ~0 rows (aproximadamente)
DELETE FROM `cliente`;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.empresa
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `nombreEmpresa` varchar(30) NOT NULL,
  `identificadorEmpresa` varchar(50) NOT NULL,
  `logoEmpresa` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idEmpresa`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.empresa: ~0 rows (aproximadamente)
DELETE FROM `empresa`;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` (`idEmpresa`, `nombreEmpresa`, `identificadorEmpresa`, `logoEmpresa`) VALUES
	(1, 'TerraBus', 'F48151818', NULL);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.estacion
DROP TABLE IF EXISTS `estacion`;
CREATE TABLE IF NOT EXISTS `estacion` (
  `idEstacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombreEstacion` varchar(50) DEFAULT NULL,
  `localidadEstacion` varchar(50) NOT NULL,
  `direccionEstacion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idEstacion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.estacion: ~4 rows (aproximadamente)
DELETE FROM `estacion`;
/*!40000 ALTER TABLE `estacion` DISABLE KEYS */;
INSERT INTO `estacion` (`idEstacion`, `nombreEstacion`, `localidadEstacion`, `direccionEstacion`) VALUES
	(1, 'Los LLanos', 'Albacete', NULL),
	(2, 'Parada Autobús Chinchilla', 'Chinchilla', NULL),
	(3, 'Parada Autobús Corral Rubio', 'Corral Rubio', NULL),
	(4, 'Parada Autobús Villar de Chinchilla', 'Villar de Chinchilla', NULL);
/*!40000 ALTER TABLE `estacion` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.horario
DROP TABLE IF EXISTS `horario`;
CREATE TABLE IF NOT EXISTS `horario` (
  `idHorario` int(11) NOT NULL AUTO_INCREMENT,
  `fkIdRutaHorario` int(11) NOT NULL,
  `horaSalidaHorario` time NOT NULL,
  `diaSemanaHorario` enum('L-V','S') NOT NULL,
  `comentario` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idHorario`),
  KEY `FK_horario_ruta` (`fkIdRutaHorario`),
  CONSTRAINT `FK_horario_ruta` FOREIGN KEY (`fkIdRutaHorario`) REFERENCES `ruta` (`idRuta`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.horario: ~22 rows (aproximadamente)
DELETE FROM `horario`;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
INSERT INTO `horario` (`idHorario`, `fkIdRutaHorario`, `horaSalidaHorario`, `diaSemanaHorario`, `comentario`) VALUES
	(1, 1, '08:00:00', 'L-V', 'Ruta Albacete-Chinchilla'),
	(2, 1, '13:00:00', 'L-V', 'Ruta Albacete-Chinchilla'),
	(3, 1, '14:20:00', 'L-V', 'Ruta Albacete-Chinchilla'),
	(4, 1, '13:30:00', 'S', 'Ruta Albacete-Chinchilla'),
	(5, 1, '20:00:00', 'S', 'Ruta Albacete-Chinchilla'),
	(6, 2, '13:00:00', 'L-V', 'Ruta Albacete-Corral Rubio'),
	(7, 3, '08:00:00', 'L-V', 'Ruta Albacete-Estacion de Chinchilla'),
	(8, 3, '13:00:00', 'L-V', 'Ruta Albacete-Estacion de Chinchilla'),
	(9, 3, '14:20:00', 'L-V', 'Ruta Albacete-Estacion de Chinchilla'),
	(10, 3, '13:30:00', 'S', 'Ruta Albacete-Estacion de Chinchilla'),
	(11, 3, '20:00:00', 'S', 'Ruta Albacete-Estacion de Chinchilla'),
	(12, 4, '07:35:00', 'L-V', 'Ruta Chinchilla-Albacete'),
	(13, 4, '09:30:00', 'L-V', 'Ruta Chinchilla-Albacete'),
	(14, 4, '13:35:00', 'L-V', 'Ruta Chinchilla-Albacete'),
	(15, 4, '10:10:00', 'S', 'Ruta Chinchilla-Albacete'),
	(16, 4, '17:45:00', 'S', 'Ruta Chinchilla-Albacete '),
	(17, 5, '13:15:00', 'L-V', 'Ruta Chinchilla-Corral Rubio'),
	(18, 6, '08:15:00', 'L-V', 'Ruta Chinchilla-Estacion de Chinchilla'),
	(19, 6, '13:15:00', 'L-V', 'Ruta Chinchilla-Estacion de Chinchilla'),
	(20, 6, '14:30:00', 'L-V', 'Ruta Chinchilla-Estacion de Chinchilla'),
	(21, 6, '10:25:00', 'S', 'Ruta Chinchilla-Estacion de Chinchilla'),
	(22, 6, '18:00:00', 'S', 'Ruta Chinchilla-Estacion de Chinchilla');
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.ocupacion
DROP TABLE IF EXISTS `ocupacion`;
CREATE TABLE IF NOT EXISTS `ocupacion` (
  `idOcupacion` int(11) NOT NULL AUTO_INCREMENT,
  `fkIdReservaOcupacion` int(11) NOT NULL,
  `fkIdViajeroOcupacion` int(11) DEFAULT NULL,
  `asientoOcupacion` int(11) NOT NULL,
  `importeIndividualOcupacion` int(11) NOT NULL,
  PRIMARY KEY (`idOcupacion`),
  UNIQUE KEY `fkIdReservaOcupacion_fkIdViajeroOcupacion_asientoOcupacion` (`fkIdReservaOcupacion`,`fkIdViajeroOcupacion`,`asientoOcupacion`),
  KEY `FK_ocupacion_reserva` (`fkIdReservaOcupacion`),
  KEY `FK_ocupacion_viajero` (`fkIdViajeroOcupacion`),
  CONSTRAINT `FK_ocupacion_reserva` FOREIGN KEY (`fkIdReservaOcupacion`) REFERENCES `reserva` (`idReserva`),
  CONSTRAINT `FK_ocupacion_viajero` FOREIGN KEY (`fkIdViajeroOcupacion`) REFERENCES `viajero` (`idViajero`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.ocupacion: ~0 rows (aproximadamente)
DELETE FROM `ocupacion`;
/*!40000 ALTER TABLE `ocupacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocupacion` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.reserva
DROP TABLE IF EXISTS `reserva`;
CREATE TABLE IF NOT EXISTS `reserva` (
  `idReserva` int(11) NOT NULL AUTO_INCREMENT,
  `fkIdViajeReserva` int(11) NOT NULL,
  `fkIdTarjetaReserva` int(11) DEFAULT NULL,
  `localizadorReserva` varchar(50) NOT NULL,
  `precioReserva` int(11) NOT NULL,
  `fechaPagoReserva` date NOT NULL,
  `numViajerosReserva` int(11) NOT NULL,
  PRIMARY KEY (`idReserva`),
  KEY `FK_reserva_viaje` (`fkIdViajeReserva`),
  KEY `FK_reserva_tarjeta` (`fkIdTarjetaReserva`),
  CONSTRAINT `FK_reserva_tarjeta` FOREIGN KEY (`fkIdTarjetaReserva`) REFERENCES `tarjeta` (`idTarjeta`),
  CONSTRAINT `FK_reserva_viaje` FOREIGN KEY (`fkIdViajeReserva`) REFERENCES `viaje` (`idViaje`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.reserva: ~0 rows (aproximadamente)
DELETE FROM `reserva`;
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.ruta
DROP TABLE IF EXISTS `ruta`;
CREATE TABLE IF NOT EXISTS `ruta` (
  `idRuta` int(11) NOT NULL AUTO_INCREMENT,
  `fkIdEstacionOrigenRuta` int(11) NOT NULL,
  `fkIdEstacionDestinoRuta` int(11) NOT NULL,
  `kmRuta` int(11) NOT NULL,
  `precioRuta` double NOT NULL,
  `duracionRuta` int(11) NOT NULL,
  `comentario` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idRuta`),
  KEY `FK_ruta_estacion` (`fkIdEstacionOrigenRuta`),
  KEY `FK_ruta_estacion_2` (`fkIdEstacionDestinoRuta`),
  CONSTRAINT `FK_ruta_estacion` FOREIGN KEY (`fkIdEstacionOrigenRuta`) REFERENCES `estacion` (`idEstacion`),
  CONSTRAINT `FK_ruta_estacion_2` FOREIGN KEY (`fkIdEstacionDestinoRuta`) REFERENCES `estacion` (`idEstacion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.ruta: ~6 rows (aproximadamente)
DELETE FROM `ruta`;
/*!40000 ALTER TABLE `ruta` DISABLE KEYS */;
INSERT INTO `ruta` (`idRuta`, `fkIdEstacionOrigenRuta`, `fkIdEstacionDestinoRuta`, `kmRuta`, `precioRuta`, `duracionRuta`, `comentario`) VALUES
	(1, 1, 2, 15, 1.4, 15, 'Albacete-Chichilla'),
	(2, 1, 3, 45, 5.45, 40, 'Albacete-Corral Rubio'),
	(3, 1, 4, 18, 1.65, 20, 'Albacete-Estación de Chinchilla'),
	(4, 2, 1, 15, 1.4, 15, 'Chinchilla-Albacete'),
	(5, 2, 3, 30, 4.05, 25, 'Chinchilla-Corral Rubio'),
	(6, 2, 4, 5, 0.15, 5, 'Chinchilla-Estación de Chinchilla');
/*!40000 ALTER TABLE `ruta` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.tarjeta
DROP TABLE IF EXISTS `tarjeta`;
CREATE TABLE IF NOT EXISTS `tarjeta` (
  `idTarjeta` int(11) NOT NULL AUTO_INCREMENT,
  `tipoTarjeta` enum('MASTERCARD','VISA') NOT NULL,
  `numeroTarjeta` varchar(50) NOT NULL,
  `caducidadTarjeta` varchar(50) NOT NULL,
  `fkIdClienteTarjeta` int(11) NOT NULL,
  `logoTarjeta` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTarjeta`),
  UNIQUE KEY `numeroTarjeta` (`numeroTarjeta`),
  KEY `FK_tarjeta_cliente` (`fkIdClienteTarjeta`),
  CONSTRAINT `FK_tarjeta_cliente` FOREIGN KEY (`fkIdClienteTarjeta`) REFERENCES `cliente` (`idCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.tarjeta: ~0 rows (aproximadamente)
DELETE FROM `tarjeta`;
/*!40000 ALTER TABLE `tarjeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarjeta` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.viaje
DROP TABLE IF EXISTS `viaje`;
CREATE TABLE IF NOT EXISTS `viaje` (
  `idViaje` int(11) NOT NULL AUTO_INCREMENT,
  `fkIdHorarioViaje` int(11) NOT NULL,
  `fechaViaje` date NOT NULL,
  `plazasLibresViaje` int(11) NOT NULL DEFAULT '8',
  PRIMARY KEY (`idViaje`),
  KEY `FK_viaje_horario` (`fkIdHorarioViaje`),
  CONSTRAINT `FK_viaje_horario` FOREIGN KEY (`fkIdHorarioViaje`) REFERENCES `horario` (`idHorario`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.viaje: ~22 rows (aproximadamente)
DELETE FROM `viaje`;
/*!40000 ALTER TABLE `viaje` DISABLE KEYS */;
INSERT INTO `viaje` (`idViaje`, `fkIdHorarioViaje`, `fechaViaje`, `plazasLibresViaje`) VALUES
	(1, 1, '2019-03-22', 8),
	(2, 2, '2019-03-22', 8),
	(3, 3, '2019-03-22', 8),
	(4, 4, '2019-03-23', 8),
	(5, 5, '2019-03-23', 8),
	(6, 6, '2019-03-22', 8),
	(7, 7, '2019-03-22', 8),
	(8, 8, '2019-03-22', 8),
	(9, 9, '2019-03-22', 8),
	(10, 10, '2019-03-23', 8),
	(11, 11, '2019-03-23', 8),
	(12, 12, '2019-03-22', 8),
	(13, 13, '2019-03-22', 8),
	(14, 14, '2019-03-22', 8),
	(15, 15, '2019-03-23', 8),
	(16, 16, '2019-03-23', 8),
	(17, 17, '2019-03-22', 8),
	(18, 18, '2019-03-22', 8),
	(19, 19, '2019-03-22', 8),
	(20, 20, '2019-03-22', 8),
	(21, 21, '2019-03-23', 8),
	(22, 22, '2019-03-23', 8);
/*!40000 ALTER TABLE `viaje` ENABLE KEYS */;

-- Volcando estructura para tabla bd_autobus_igt_2018.viajero
DROP TABLE IF EXISTS `viajero`;
CREATE TABLE IF NOT EXISTS `viajero` (
  `idViajero` int(11) NOT NULL AUTO_INCREMENT,
  `tipoIdentificadorViajero` enum('NIF','NIE','PASAPORTE') NOT NULL,
  `identificadorViajero` varchar(50) NOT NULL,
  `nombreViajero` varchar(50) NOT NULL,
  `apellidosViajero` varchar(50) NOT NULL,
  PRIMARY KEY (`idViajero`),
  UNIQUE KEY `identificadorViajero` (`identificadorViajero`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bd_autobus_igt_2018.viajero: ~0 rows (aproximadamente)
DELETE FROM `viajero`;
/*!40000 ALTER TABLE `viajero` DISABLE KEYS */;
/*!40000 ALTER TABLE `viajero` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
